import pandas as pd
import os

# Paths to the dataset and CSV file
parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/Dutch-PD"
output_csv_separate = "/gpfsdswork/projects/rech/fmr/uft12cr/Dutch_PD_deduplication.csv"

# Load the CSV file with identified duplicates
duplicates_df = pd.read_csv(output_csv_separate)

# Get the list of unique duplicated file IDs
duplicated_file_ids = pd.concat([duplicates_df['file_id1'], duplicates_df['file_id2']]).unique()

# Count the number of duplicated files
num_duplicated_files = len(duplicated_file_ids)

# Load all parquet files to get the total number of unique file IDs in the complete dataset
file_ids = []
for file_name in os.listdir(parquet_dir_fr):
    if file_name.endswith(".parquet"):
        file_path = os.path.join(parquet_dir_fr, file_name)
        df = pd.read_parquet(file_path)
        file_ids.extend(df['identifier'].unique())

# Get the total number of unique file IDs
total_unique_file_ids = len(set(file_ids))

# Calculate the ratio/percentage of deduplicated files
ratio_deduplicated = num_duplicated_files / total_unique_file_ids
percentage_deduplicated = ratio_deduplicated * 100

print(f"Number of duplicated files: {num_duplicated_files}")
print(f"Total number of unique file IDs in the complete dataset: {total_unique_file_ids}")
print(f"Ratio of deduplicated files: {ratio_deduplicated:.2f}")
print(f"Percentage of deduplicated files: {percentage_deduplicated:.2f}%")
