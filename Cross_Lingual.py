import pandas as pd
import os
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import pairwise_distances
import time
import psutil
from dask import delayed, compute
import dask.array as da
import pyarrow.parquet as pq


def process_parquet_file(file_path, text_column, chunksize=200):
    chunks = []
    parquet_file = pq.ParquetFile(file_path)
    for batch in parquet_file.iter_batches(batch_size=chunksize):
        chunk = batch.to_pandas()
        chunk['complete_text'] = chunk[text_column].str.lower()
        chunk['complete_text'] = chunk['complete_text'].str.replace('[^\w\s]', '', regex=True)
        chunks.append(chunk)
    return chunks


def sample_long_texts(chunks):
    sampled_chunks = []
    for chunk in chunks:
        sampled_chunk = chunk[chunk['complete_text'].str.len() > 1000].sample(n=10, replace=True)
        sampled_chunks.append(sampled_chunk)
    return sampled_chunks


def warn_memory_usage():
    mem_usage = psutil.virtual_memory().percent
    if mem_usage > 20:
        print(f"Warning: Memory usage is high ({mem_usage}%)")


def tfidf_representation(documents, vectorizer=None):
    processed_documents = (doc for doc in documents if doc.strip() != "")
    if not processed_documents:
        print("No valid documents to vectorize.")
        return None

    if vectorizer is None:
        vectorizer = TfidfVectorizer(max_features=2**16, dtype=np.float32)
        tfidf_matrix = vectorizer.fit_transform(processed_documents)
    else:
        tfidf_matrix = vectorizer.transform(processed_documents)

    return tfidf_matrix, vectorizer


@delayed
def process_chunk(chunk_docs, vectorizer, chunk_size):
    embeddings, _ = tfidf_representation(chunk_docs, vectorizer)
    embeddings = embeddings.toarray()  # Convert sparse matrix to dense matrix
    similarity_matrix = pairwise_distances(embeddings, metric='jaccard', n_jobs=-1)
    return da.from_array(similarity_matrix, chunks=(chunk_size, chunk_size))


def cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, threshold=0.5, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='file_id', title_column_diverse='title', output_csv="output_cross_lingual.csv", chunk_size=200):
    common_documents = []
    start_time = time.time()

    files_fr = [f for f in os.listdir(parquet_dir_fr) if f.endswith(".parquet")]
    files_diverse = [f for f in os.listdir(parquet_dir_diverse) if f.endswith(".parquet")]

    vectorizer = TfidfVectorizer(max_features=2**16, dtype=np.float32)

    with open(output_csv, 'w') as f:
        f.write("file_id_fr,title_fr,file_id_diverse,title_diverse,similarity_score\n")

    for file_name_fr in files_fr:
        file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
        chunks_fr = process_parquet_file(file_path_fr, 'complete_text', chunksize=chunk_size)
        sampled_chunks_fr = sample_long_texts(chunks_fr)

        for file_name_diverse in files_diverse:
            file_path_diverse = os.path.join(parquet_dir_diverse, file_name_diverse)
            chunks_diverse = process_parquet_file(file_path_diverse, 'complete_text', chunksize=chunk_size)
            sampled_chunks_diverse = sample_long_texts(chunks_diverse)

            for chunk_fr in sampled_chunks_fr:
                documents_fr = chunk_fr['complete_text'].tolist()
                vectorizer.fit(documents_fr)

                delayed_results_fr = []
                for i in range(0, len(documents_fr), chunk_size):
                    chunk_docs_fr = documents_fr[i:i+chunk_size]
                    delayed_results_fr.append(process_chunk(chunk_docs_fr, vectorizer, chunk_size))

                similarity_matrices_fr = compute(*delayed_results_fr)

                for chunk_diverse in sampled_chunks_diverse:
                    documents_diverse = chunk_diverse['complete_text'].tolist()
                    vectorizer.fit(documents_diverse)

                    delayed_results_diverse = []
                    for j in range(0, len(documents_diverse), chunk_size):
                        chunk_docs_diverse = documents_diverse[j:j+chunk_size]
                        delayed_results_diverse.append(process_chunk(chunk_docs_diverse, vectorizer, chunk_size))

                    similarity_matrices_diverse = compute(*delayed_results_diverse)

                    for similarity_matrix_fr, similarity_matrix_diverse in zip(similarity_matrices_fr, similarity_matrices_diverse):
                        similarity_matrix_fr = similarity_matrix_fr.compute_chunk_sizes()
                        similarity_matrix_diverse = similarity_matrix_diverse.compute_chunk_sizes()

                        combined_similarity_matrix = np.concatenate((similarity_matrix_fr, similarity_matrix_diverse), axis=1)
                        combined_similarity_matrix = combined_similarity_matrix.compute()

                        common_indices = np.argwhere(combined_similarity_matrix > threshold)
                        file_ids_fr = chunk_fr[file_id_column_fr].tolist()
                        titles_fr = chunk_fr[title_column_fr].tolist()
                        file_ids_diverse = chunk_diverse[file_id_column_diverse].tolist()
                        titles_diverse = chunk_diverse[title_column_diverse].tolist()

                        for idx_pair in common_indices:
                            fr_idx = idx_pair[0]
                            diverse_idx = idx_pair[1]

                            if diverse_idx < len(file_ids_diverse):
                                file_id_fr = file_ids_fr[fr_idx]
                                title_fr = titles_fr[fr_idx]
                                file_id_diverse = file_ids_diverse[diverse_idx]
                                title_diverse = titles_diverse[diverse_idx]
                                similarity_score = combined_similarity_matrix[fr_idx, diverse_idx]
                                common_documents.append((file_id_fr, title_fr, file_id_diverse, title_diverse, similarity_score))
                                with open(output_csv, 'a') as f:
                                    f.write(f"{file_id_fr},{title_fr},{file_id_diverse},{title_diverse},{similarity_score}\n")
                    warn_memory_usage()

    end_time = time.time()
    duration = end_time - start_time
    print(f"Cross-lingual deduplication process took {duration} seconds.")
    return duration


# Example usage:
parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books"
parquet_dir_diverse = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-diverse"
output_csv_cross_lingual = "/gpfsdswork/projects/rech/fmr/uft12cr/cross_lingual_deduplication_PD_Diverse_CompleteDataset.csv"

cross_lingual_start_time = time.time()
cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, threshold=0.8, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='identifier', title_column_diverse='title', output_csv=output_csv_cross_lingual, chunk_size=200)
cross_lingual_end_time = time.time()

cross_lingual_total_duration = cross_lingual_end_time - cross_lingual_start_time
print(f"Total cross-lingual similarity process took {cross_lingual_total_duration} seconds.")
