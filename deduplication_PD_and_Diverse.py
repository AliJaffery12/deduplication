import subprocess
import psutil
import resource
import pandas as pd
import os

import numpy as np
from sklearn.metrics.pairwise import cosine_similarity
from sentence_transformers import SentenceTransformer
from scipy.spatial.transform import Rotation as R

import time
import nltk
from nltk.corpus import words,stopwords

nltk.download('words')
nltk.download('stopwords')

english_words = set(words.words())
french_stopwords = set(stopwords.words('french'))



def process_parquet_file(file_path, text_column):
    parquet_df = pd.read_parquet(file_path)
    parquet_df['complete_text'] = parquet_df[text_column].str.lower()  
    parquet_df['complete_text'] = parquet_df['complete_text'].str.replace('[^\w\s]', '', regex=True)  
    return parquet_df


def sample_long_texts(parquet_df):
    sampled_df = parquet_df[parquet_df['complete_text'].str.len() > 1000].sample(n=10, replace=True)
    return sampled_df


def compute_document_embeddings(documents, model):
    embeddings = model.encode(documents)
    return embeddings

def warn_memory_usage():
    mem_usage = psutil.virtual_memory().percent
    if mem_usage > 20:
        print(f"Warning: Memory usage is high ({mem_usage}%)")


def estimate_ocr_quality(sample_df, id_column, text_column):
    ocr_quality_estimation = []
    for index, row in sample_df.iterrows():
        complete_text = row[text_column]
        
        # Non-word characters
        non_word_chars = len([c for c in complete_text if not c.isalnum() and not c.isspace()])
        non_word_char_freq = non_word_chars / len(complete_text)
        
        # Out-of-vocabulary words
        words = complete_text.split()
        oov_words = [word for word in words if word.lower() not in french_stopwords]
        oov_word_freq = len(oov_words) / len(words)
        
        # Tokenization errors
        tokenization_errors = len([word for word in words if len(word) > 1 and word[0] == r"'"])
        tokenization_error_freq = tokenization_errors / len(words)
        
        ocr_quality_estimation.append((row[id_column], row['title'], non_word_char_freq, oov_word_freq, tokenization_error_freq))
    
    ocr_quality_df = pd.DataFrame(ocr_quality_estimation, columns=[id_column, 'title', 'non_word_char_freq', 'oov_word_freq', 'tokenization_error_freq'])
    return ocr_quality_df


def minhash_deduplication(sample_df, threshold, model, file_id_column, title_column, output_csv):
    start_time = time.time()
    warn_memory_usage()
    print("Sampled DataFrame shape:", sample_df.shape)
    documents = sample_df['complete_text'].tolist()
    embeddings = compute_document_embeddings(documents, model)

    similarity_matrix = cosine_similarity(embeddings)
    print("Similarity matrix shape:", similarity_matrix.shape)  

    duplicates = np.argwhere((similarity_matrix > threshold) & (np.arange(len(similarity_matrix))[:, None]!= np.arange(len(similarity_matrix))))
    print("Number of duplicates found:", len(duplicates))  

    file_ids = sample_df[file_id_column].tolist()
    titles = sample_df[title_column].tolist()

    ranked_duplicates = []
    for pair in duplicates:
        file_id1 = file_ids[pair[0]]
        file_id2 = file_ids[pair[1]]
        title1 = titles[pair[0]]
        title2 = titles[pair[1]]
        similarity_score = similarity_matrix[pair[0], pair[1]]
        ranked_duplicates.append((file_id1, file_id2, similarity_score, title1, title2))

    if ranked_duplicates:
        df = pd.DataFrame(ranked_duplicates, columns=['file_id1', 'file_id2', 'similarity_score', 'title1', 'title2'])
        df.to_csv(output_csv, index=False)  
        print(f"Deduplication results saved to {output_csv}")
    else:
        print("No duplicates found.")

    for file_id1, file_id2, similarity_score, title1, title2 in ranked_duplicates:
        print(f"Files '{file_id1}' and '{file_id2}' are similar with score {similarity_score}. Metadata: {title1}, {title2}")

    ocr_quality_df = estimate_ocr_quality(sample_df, file_id_column, 'complete_text')
    ocr_quality_df.to_csv(output_csv.replace('.csv', '_ocr_quality.csv'), index=False)
    print(f"OCR quality estimation results saved to {output_csv.replace('.csv', '_ocr_quality.csv')}")

    end_time = time.time()  # Record end time
    duration = end_time - start_time  
    print(f"Deduplication process took {duration} seconds.")
    return duration

    
    

    

def rotation_alignment(embeddings_fr, embeddings_diverse):
    #print("Applying rotation-based alignment...")
    
    covariance_matrix = np.dot(embeddings_fr.T, embeddings_diverse)
    
    
    U, _, Vt = np.linalg.svd(covariance_matrix)
    
  
    rotation_matrix = np.dot(U, Vt)
    
    
    aligned_embeddings_diverse = np.dot(embeddings_diverse, rotation_matrix.T)
    
    return aligned_embeddings_diverse

thresholds = [ 0.8]

model = SentenceTransformer('xlm-r-100langs-bert-base-nli-mean-tokens')


parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books"
parquet_dir_diverse = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-diverse"

output_csv_separate = "/home/k54765/deduplication_results_frenchPD.csv"
output_csv_separate_it = "/home/k54765/deduplication_results_Frech_Diverse.csv"
output_csv_cross_lingual = "/home/k54765/cross_lingual_deduplication_PD_Diverse.csv"

for file_name_fr in os.listdir(parquet_dir_fr):
    if file_name_fr.endswith(".parquet"):
        file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
        parquet_df_fr = process_parquet_file(file_path_fr, 'complete_text')
        sampled_df_fr = sample_long_texts(parquet_df_fr)
        
        for threshold in thresholds:
              warn_memory_usage()
              minhash_deduplication(sampled_df_fr, threshold, model, file_id_column='file_id', title_column='title', output_csv=output_csv_separate)
              

for file_name_diverse in os.listdir(parquet_dir_diverse):
    if file_name_diverse.endswith(".parquet"):
        file_path_it = os.path.join(parquet_dir_diverse, file_name_diverse)
        parquet_df_it = process_parquet_file(file_path_it, 'complete_text')
        sampled_df_it = sample_long_texts(parquet_df_it)
        
        for threshold in thresholds:
             
             warn_memory_usage()
             minhash_deduplication(sampled_df_it, threshold, model, file_id_column='identifier', title_column='title', output_csv=output_csv_separate_it)
             

def cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, model, threshold=0.5, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='file_id', title_column_diverse='title',output_csv=output_csv_cross_lingual):
    common_documents = []
    start_time = time.time() 
    for file_name_fr in os.listdir(parquet_dir_fr):
        if file_name_fr.endswith(".parquet"):
            file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
            parquet_df_fr = process_parquet_file(file_path_fr, 'complete_text')
            sampled_df_fr = sample_long_texts(parquet_df_fr)

            for file_name_diverse in os.listdir(parquet_dir_diverse):
                if file_name_diverse.endswith(".parquet"):
                    file_path_it = os.path.join(parquet_dir_diverse, file_name_diverse)
                    parquet_df_it = process_parquet_file(file_path_it, 'complete_text')
                    sampled_df_it = sample_long_texts(parquet_df_it)

                    embeddings_fr = model.encode(sampled_df_fr['complete_text'].tolist())
                    embeddings_diverse = model.encode(sampled_df_it['complete_text'].tolist())

                    # Apply rotation-based alignment
                    aligned_embeddings_diverse = rotation_alignment(embeddings_fr, embeddings_diverse)

                    similarity_matrix = cosine_similarity(embeddings_fr, aligned_embeddings_diverse)

                    common_indices = np.argwhere(similarity_matrix > threshold)

                    file_ids_fr = sampled_df_fr[file_id_column_fr].tolist()
                    titles_fr = sampled_df_fr[title_column_fr].tolist()
                    file_ids_it = sampled_df_it[file_id_column_diverse].tolist()
                    titles_it = sampled_df_it[title_column_diverse].tolist()

                    for idx_pair in common_indices:
                        fr_idx = idx_pair[0]
                        it_idx = idx_pair[1]
                        file_id_fr = file_ids_fr[fr_idx]
                        title_fr = titles_fr[fr_idx]
                        file_id_it = file_ids_it[it_idx]
                        title_it = titles_it[it_idx]
                        similarity_score = similarity_matrix[fr_idx, it_idx]
                        common_documents.append((file_id_fr, title_fr, file_id_it, title_it, similarity_score))

    df = pd.DataFrame(common_documents, columns=['file_id_fr', 'title_fr', 'file_id_PD_Diverse', 'title_PD_Diverse', 'similarity_score'])
    df.to_csv(output_csv, index=False)  # Save to CSV
    print("Common French_PD/French_PD Diverse documents:")
    for file_id_fr, title_fr, file_id_it, title_it, similarity_score in common_documents:
        print(f"French_PD document '{file_id_fr}' ({title_fr}) is similar to French_PD_Diverse document '{file_id_it}' ({title_it}) with similarity score {similarity_score}")
    end_time = time.time()  # Record end time
    duration = end_time - start_time  # Calculate duration
    print(f"Deduplication process took {duration} seconds.")
    return duration


cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, model, threshold=0.7, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='identifier', title_column_diverse='title')