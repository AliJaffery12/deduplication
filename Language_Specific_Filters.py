import os
import pandas as pd
import pyarrow.parquet as pq
from collections import defaultdict
import re
import json

base_path = "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/big_code_1/data"

def categorize_language(lang, ext):
    if lang is None and (ext is None or ext == ""):
        return "Unknown"
    
    lang = str(lang).lower() if lang else ""
    ext = str(ext).lower() if ext and ext != "" else ""

    # First, check based on language name
    if any(keyword in lang for keyword in ["markup", "xml", "html", "sgml", "tex"]):
        return "Markup Languages"
    elif any(keyword in lang for keyword in ["script", "shell", "bash", "powershell"]):
        return "Scripting Languages"
    elif any(keyword in lang for keyword in ["sql", "query"]):
        return "Query Languages"
    elif any(keyword in lang for keyword in ["config", "ini", "yaml", "toml", "json"]):
        return "Configuration Languages"
    elif any(keyword in lang for keyword in ["c", "cpp", "java", "python", "ruby", "go", "rust", "swift"]):
        return "General Purpose Languages"
    elif any(keyword in lang for keyword in ["web", "css", "scss", "less", "js", "javascript", "typescript"]):
        return "Web Technologies"
    elif any(keyword in lang for keyword in ["functional", "haskell", "lisp", "scheme", "clojure", "erlang"]):
        return "Functional Programming Languages"
    elif any(keyword in lang for keyword in ["assembly", "asm"]):
        return "Low-Level Languages"
    elif any(keyword in lang for keyword in ["matlab", "r", "julia", "fortran", "octave"]):
        return "Scientific Computing Languages"

    # If language name doesn't give enough information, check extension
    if ext:
        if ext in [".html", ".xml", ".sgml", ".tex"]:
            return "Markup Languages"
        elif ext in [".sh", ".bash", ".ps1"]:
            return "Scripting Languages"
        elif ext in [".sql"]:
            return "Query Languages"
        elif ext in [".ini", ".yaml", ".toml", ".json", ".conf"]:
            return "Configuration Languages"
        elif ext in [".py", ".java", ".cpp", ".c", ".rb", ".go", ".rs", ".swift"]:
            return "General Purpose Languages"
        elif ext in [".css", ".scss", ".less", ".js", ".ts"]:
            return "Web Technologies"
        elif ext in [".hs", ".lisp", ".scm", ".clj", ".erl"]:
            return "Functional Programming Languages"
        elif ext in [".asm", ".s"]:
            return "Low-Level Languages"
        elif ext in [".m", ".r", ".jl", ".f", ".f90"]:
            return "Scientific Computing Languages"
        elif ext in [".mak", ".cmake", ".gradle", ".build"]:
            return "Build Systems"
    
    return "Other Languages"

def apply_language_specific_filters(content, filename, category):
    if category not in ["Markup Languages", "Configuration Languages"]:
        return True, None

    # Handle potential None values
    if content is None:
        return False, "No content"
    if filename is None:
        return False, "No filename"

    # Filter 1: Remove files with more than 512 lines
    if len(content.splitlines()) > 512:
        return False, "More than 512 lines"

    # Filter 2: Keep only files where visible text is at least 100 characters long and makes up at least 20% of the code
    visible_text = re.sub(r'<[^>]+>', '', content)  # Remove HTML tags
    if len(visible_text) < 100 or len(visible_text) / len(content) < 0.2:
        return False, "Insufficient visible text"

    # Filter 3: Keep only files with specific filenames
    filename_lower = filename.lower()
    filename_without_ext = os.path.splitext(filename_lower)[0]
    if "requirement" in filename_lower or filename_without_ext in {"readme", "notes", "todo", "description", "cmakelists"}:
        return True, None
    
    return False, "Filename doesn't match criteria"

def analyze_folder(folder_path):
    category_counts = defaultdict(int)
    language_counts = defaultdict(int)
    extension_counts = defaultdict(int)
    filter_results = defaultdict(lambda: defaultdict(int))
    
    for file in os.listdir(folder_path):
        if file.endswith('.parquet'):
            file_path = os.path.join(folder_path, file)
            parquet_file = pq.ParquetFile(file_path)
            
            # Check available columns
            available_columns = parquet_file.schema.names
            required_columns = ['lang', 'ext', 'content']
            optional_columns = ['path']
            
            # Ensure all required columns are present
            if not all(col in available_columns for col in required_columns):
                print(f"Skipping file {file_path} due to missing required columns")
                continue
            
            # Use 'path' column if available, otherwise use filename
            columns_to_read = required_columns + [col for col in optional_columns if col in available_columns]
            
            for batch in parquet_file.iter_batches(batch_size=10000, columns=columns_to_read):
                df = batch.to_pandas()
                
                for index, row in df.iterrows():
                    lang = row['lang']
                    ext = row['ext']
                    content = row['content']
                    path = row['path'] if 'path' in df.columns else file
                    
                    category = categorize_language(lang, ext)
                    filename = os.path.basename(path)

                    # Apply language-specific filters
                    passed_filter, filter_reason = apply_language_specific_filters(content, filename, category)
                    
                    if passed_filter:
                        category_counts[category] += 1
                        language_counts[lang] += 1
                        extension_counts[ext if ext else 'None'] += 1
                    else:
                        filter_results[category][filter_reason] += 1
    
    return dict(category_counts), dict(language_counts), dict(extension_counts), dict(filter_results)

all_folders = os.listdir(base_path)
folder_analysis = {}
filter_analysis = {}

for folder in all_folders:
    folder_path = os.path.join(base_path, folder)
    if os.path.isdir(folder_path):
        print(f"Processing folder: {folder}")
        category_counts, language_counts, extension_counts, filter_results = analyze_folder(folder_path)
        folder_analysis[folder] = {
            'categories': category_counts, 
            'languages': language_counts, 
            'extensions': extension_counts
        }
        filter_analysis[folder] = filter_results

# Print and save the results
with open('dynamic_language_analysis_results.txt', 'w') as f:
    for folder, data in folder_analysis.items():
        output = f"\nFolder: {folder}\n"
        output += "Categories:\n"
        for category, count in sorted(data['categories'].items(), key=lambda x: x[1], reverse=True):
            output += f"  {category}: {count}\n"
        
        output += "\nTop 10 Languages:\n"
        for lang, count in sorted(data['languages'].items(), key=lambda x: x[1], reverse=True)[:10]:
            lang_str = 'None' if lang is None else lang
            output += f"  {lang_str}: {count}\n"
        
        output += "\nTop 10 Extensions:\n"
        for ext, count in sorted(data['extensions'].items(), key=lambda x: x[1], reverse=True)[:10]:
            output += f"  {ext}: {count}\n"
        
        output += f"\nNumber of unique categories: {len(data['categories'])}\n"
        output += f"Number of unique languages: {len(data['languages'])}\n"
        output += f"Number of unique extensions: {len(data['extensions'])}\n"
        output += "=" * 50 + "\n"
        
        print(output)
        f.write(output)

# Save filter results
with open('filter_results.json', 'w') as f:
    json.dump(filter_analysis, f, indent=2)

print("Filter results saved to filter_results.json")