import os
import pandas as pd
import pyarrow.parquet as pq

base_path = "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/big_code_1/data"

def analyze_folder(folder_path):
    extensions = {}
    languages = {}
    
    for file in os.listdir(folder_path):
        if file.endswith('.parquet'):
            file_path = os.path.join(folder_path, file)
            parquet_file = pq.ParquetFile(file_path)
            
            for batch in parquet_file.iter_batches(batch_size=10000, columns=['ext', 'lang']):
                df = batch.to_pandas()
                
                # Count occurrences of each extension, including None
                ext_counts = df['ext'].value_counts(dropna=False)
                for ext, count in ext_counts.items():
                    extensions[ext] = extensions.get(ext, 0) + count
                
                # Count occurrences of each language, including None
                lang_counts = df['lang'].value_counts(dropna=False)
                for lang, count in lang_counts.items():
                    languages[lang] = languages.get(lang, 0) + count
    
    return extensions, languages

all_folders = os.listdir(base_path)
folder_analysis = {}

for folder in all_folders:
    folder_path = os.path.join(base_path, folder)
    if os.path.isdir(folder_path):
        print(f"Processing folder: {folder}")
        exts, langs = analyze_folder(folder_path)
        folder_analysis[folder] = {'extensions': exts, 'languages': langs}

# Print the results
for folder, data in folder_analysis.items():
    print(f"Folder: {folder}")
    
    print("Extensions:")
    for ext, count in sorted(data['extensions'].items(), key=lambda x: x[1], reverse=True):
        ext_str = 'None' if ext is None else ext
        print(f"  {ext_str}: {count}")
    
    print("Languages:")
    for lang, count in sorted(data['languages'].items(), key=lambda x: x[1], reverse=True):
        lang_str = 'None' if lang is None else lang
        print(f"  {lang_str}: {count}")
    
    print(f"Number of unique extensions: {len(data['extensions'])}")
    print(f"Number of unique languages: {len(data['languages'])}")
    print()