import os
import pandas as pd
import pyarrow.parquet as pq
from collections import defaultdict

base_path = "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/big_code_1/data"

def categorize_language(lang, ext):
    if lang is None and (ext is None or ext == ""):
        return "Unknown"
    
    lang = str(lang).lower() if lang else ""
    ext = str(ext).lower() if ext and ext != "" else ""

    # First, check based on language name
    if any(keyword in lang for keyword in ["markup", "xml", "html", "sgml", "tex"]):
        return "Markup Languages"
    elif any(keyword in lang for keyword in ["script", "shell", "bash", "powershell"]):
        return "Scripting Languages"
    elif any(keyword in lang for keyword in ["sql", "query"]):
        return "Query Languages"
    elif any(keyword in lang for keyword in ["config", "ini", "yaml", "toml", "json"]):
        return "Configuration Languages"
    elif any(keyword in lang for keyword in ["c", "cpp", "java", "python", "ruby", "go", "rust", "swift"]):
        return "General Purpose Languages"
    elif any(keyword in lang for keyword in ["web", "css", "scss", "less", "js", "javascript", "typescript"]):
        return "Web Technologies"
    elif any(keyword in lang for keyword in ["functional", "haskell", "lisp", "scheme", "clojure", "erlang"]):
        return "Functional Programming Languages"
    elif any(keyword in lang for keyword in ["assembly", "asm"]):
        return "Low-Level Languages"
    elif any(keyword in lang for keyword in ["matlab", "r", "julia", "fortran", "octave"]):
        return "Scientific Computing Languages"

    # If language name doesn't give enough information, check extension
    if ext:
        if ext in [".html", ".xml", ".sgml", ".tex"]:
            return "Markup Languages"
        elif ext in [".sh", ".bash", ".ps1"]:
            return "Scripting Languages"
        elif ext in [".sql"]:
            return "Query Languages"
        elif ext in [".ini", ".yaml", ".toml", ".json", ".conf"]:
            return "Configuration Languages"
        elif ext in [".py", ".java", ".cpp", ".c", ".rb", ".go", ".rs", ".swift"]:
            return "General Purpose Languages"
        elif ext in [".css", ".scss", ".less", ".js", ".ts"]:
            return "Web Technologies"
        elif ext in [".hs", ".lisp", ".scm", ".clj", ".erl"]:
            return "Functional Programming Languages"
        elif ext in [".asm", ".s"]:
            return "Low-Level Languages"
        elif ext in [".m", ".r", ".jl", ".f", ".f90"]:
            return "Scientific Computing Languages"
        elif ext in [".mak", ".cmake", ".gradle", ".build"]:
            return "Build Systems"
    
    return "Other Languages"

def should_filter_file(lang, avg_line_length, max_line_length, num_lines):
    # General filters for all languages
    if num_lines > 100000:
        return True

    # Specific filters for non-exempt languages
    exempt_languages = ["html", "json", "markdown", "roff", "tex", "xml"]
    if lang.lower() not in exempt_languages:
        if avg_line_length > 100 or max_line_length > 1000:
            return True
    else:
        # For exempt languages, only filter if max line length exceeds 100k
        if max_line_length > 100000:
            return True

    return False

def analyze_folder(folder_path):
    category_counts = defaultdict(int)
    language_counts = defaultdict(int)
    extension_counts = defaultdict(int)
    filtered_category_counts = defaultdict(int)
    filtered_language_counts = defaultdict(int)
    filtered_extension_counts = defaultdict(int)
    filtered_counts = defaultdict(int)
    
    for file in os.listdir(folder_path):
        if file.endswith('.parquet'):
            file_path = os.path.join(folder_path, file)
            parquet_file = pq.ParquetFile(file_path)
            
            for batch in parquet_file.iter_batches(batch_size=10000, columns=['lang', 'ext', 'avg_line_length', 'max_line_length', 'size']):
                df = batch.to_pandas()
                
                for lang, ext, avg_line_length, max_line_length, size in zip(
                    df['lang'], df['ext'], df['avg_line_length'], df['max_line_length'], df['size']
                ):
                    category = categorize_language(lang, ext)
                    
                    # Count before filtering
                    category_counts[category] += 1
                    language_counts[lang] += 1
                    extension_counts[ext if ext else 'None'] += 1
                    
                    # Estimate number of lines
                    num_lines = size // max(1, avg_line_length)  # Avoid division by zero
                    
                    if should_filter_file(lang, avg_line_length, max_line_length, num_lines):
                        filtered_counts[lang] += 1
                    else:
                        filtered_category_counts[category] += 1
                        filtered_language_counts[lang] += 1
                        filtered_extension_counts[ext if ext else 'None'] += 1
    
    return (dict(category_counts), dict(language_counts), dict(extension_counts), 
            dict(filtered_category_counts), dict(filtered_language_counts), 
            dict(filtered_extension_counts), dict(filtered_counts))

all_folders = os.listdir(base_path)
folder_analysis = {}

for folder in all_folders:
    folder_path = os.path.join(base_path, folder)
    if os.path.isdir(folder_path):
        print(f"Processing folder: {folder}")
        (category_counts, language_counts, extension_counts, 
         filtered_category_counts, filtered_language_counts, 
         filtered_extension_counts, filtered_counts) = analyze_folder(folder_path)
        folder_analysis[folder] = {
            'categories': category_counts,
            'languages': language_counts,
            'extensions': extension_counts,
            'filtered_categories': filtered_category_counts,
            'filtered_languages': filtered_language_counts,
            'filtered_extensions': filtered_extension_counts,
            'filtered': filtered_counts
        }

# Function to generate output string
def generate_output(data, is_filtered=False):
    prefix = "Filtered " if is_filtered else ""
    output = f"{prefix}Categories:\n"
    for category, count in sorted(data['categories'].items(), key=lambda x: x[1], reverse=True):
        output += f"  {category}: {count}\n"
    
    output += f"\nTop 10 {prefix}Languages:\n"
    for lang, count in sorted(data['languages'].items(), key=lambda x: x[1], reverse=True)[:10]:
        lang_str = 'None' if lang is None else lang
        output += f"  {lang_str}: {count}\n"
    
    output += f"\nTop 10 {prefix}Extensions:\n"
    for ext, count in sorted(data['extensions'].items(), key=lambda x: x[1], reverse=True)[:10]:
        output += f"  {ext}: {count}\n"
    
    if is_filtered:
        output += "\nFiltered Files:\n"
        for lang, count in sorted(data['filtered'].items(), key=lambda x: x[1], reverse=True):
            output += f"  {lang}: {count}\n"
    
    output += f"\nNumber of unique {prefix.lower()}categories: {len(data['categories'])}\n"
    output += f"Number of unique {prefix.lower()}languages: {len(data['languages'])}\n"
    output += f"Number of unique {prefix.lower()}extensions: {len(data['extensions'])}\n"
    if is_filtered:
        output += f"Number of filtered languages: {len(data['filtered'])}\n"
    output += "=" * 50 + "\n"
    return output

# Print and save the results
with open('categorization_results.txt', 'w') as f_cat, open('filtered_analysis_results.txt', 'w') as f_filt:
    for folder, data in folder_analysis.items():
        # Categorization results
        cat_output = f"\nFolder: {folder}\n"
        cat_output += generate_output({
            'categories': data['categories'],
            'languages': data['languages'],
            'extensions': data['extensions']
        })
        print(cat_output)
        f_cat.write(cat_output)

        # Filtered analysis results
        filt_output = f"\nFolder: {folder}\n"
        filt_output += generate_output({
            'categories': data['filtered_categories'],
            'languages': data['filtered_languages'],
            'extensions': data['filtered_extensions'],
            'filtered': data['filtered']
        }, is_filtered=True)
        print(filt_output)
        f_filt.write(filt_output)