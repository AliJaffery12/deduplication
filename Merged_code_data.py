import os
import pandas as pd
import json
import numpy as np

# Define the base paths
v1_base = '/gpfsscratch/rech/fmr/uft12cr/corpus/code/the-stack-v1-dedup-filt/data'
v2_base = '/gpfsscratch/rech/fmr/uft12cr/corpus/code/the-stack-v2-dedup-sample/data'
output_base = '/gpfsscratch/rech/fmr/uft12cr/corpus/code/merged_datasets'

# Create the output directory if it doesn't exist
os.makedirs(output_base, exist_ok=True)

def check_columns(language):
    v1_path = os.path.join(v1_base, language)
    v2_path = os.path.join(v2_base, language)
    all_columns = {}

    def update_column_info(df):
        for col in df.columns:
            if col not in all_columns:
                all_columns[col] = set()
            all_columns[col].add(str(df[col].dtype))

    # Check v1 files
    if os.path.exists(v1_path):
        for root, _, files in os.walk(v1_path):
            for file in files:
                if file.endswith('.parquet'):
                    df = pd.read_parquet(os.path.join(root, file))
                    update_column_info(df)

    # Check v2 files
    if os.path.exists(v2_path):
        for file in os.listdir(v2_path):
            if file.endswith('.parquet'):
                df = pd.read_parquet(os.path.join(v2_path, file))
                update_column_info(df)

    print(f"Columns found in {language} datasets:")
    for col, types in sorted(all_columns.items()):
        print(f"- {col}: {', '.join(types)}")

    return list(all_columns.keys())
    
def numpy_to_list(obj):
    if isinstance(obj, np.ndarray):
        return obj.tolist()
    elif isinstance(obj, dict):
        return {k: numpy_to_list(v) for k, v in obj.items()}
    elif isinstance(obj, list):
        return [numpy_to_list(i) for i in obj]
    else:
        return obj

def merge_and_filter_language(language):
    dfs = []
    columns = check_columns(language)

    # Read v1 files
    v1_path = os.path.join(v1_base, language)
    if os.path.exists(v1_path):
        for root, _, files in os.walk(v1_path):
            for file in files:
                if file.endswith('.parquet'):
                    df = pd.read_parquet(os.path.join(root, file))
                    dfs.append(df)

    # Read v2 files
    v2_path = os.path.join(v2_base, language)
    if os.path.exists(v2_path):
        for file in os.listdir(v2_path):
            if file.endswith('.parquet'):
                df = pd.read_parquet(os.path.join(v2_path, file))
                dfs.append(df)

    # Merge all dataframes
    if dfs:
        merged_df = pd.concat(dfs, ignore_index=True)

        # Convert any numpy array, dict, or list columns to JSON-serializable format
        for col in merged_df.columns:
            if merged_df[col].dtype == 'object':
                merged_df[col] = merged_df[col].apply(lambda x: json.dumps(numpy_to_list(x)) if isinstance(x, (dict, list, np.ndarray)) else str(x))

        # Remove duplicates
        merged_df = merged_df.astype(str).drop_duplicates()

        # Filter for permissive license if the column exists
        if 'license_type' in columns:
            merged_df = merged_df[merged_df['license_type'] == 'permissive']
        else:
            print("Warning: 'license_type' column not found. Skipping license filtering.")

        # Save the filtered dataset
        output_path = os.path.join(output_base, f'{language}_merged.parquet')
        merged_df.to_parquet(output_path, index=False)
        print(f"Merged and filtered {language} dataset saved to {output_path}")
        print(f"Final dataset shape: {merged_df.shape}")
    else:
        print(f"No data found for {language}")

# Process COBOL and R languages
merge_and_filter_language('COBOL')
merge_and_filter_language('R')
print("Merging and filtering complete.")