import pandas as pd
import os
import numpy as np
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import pairwise_distances
import time
import psutil
from dask import delayed, compute
from scipy.sparse import csr_matrix
import dask.array as da
import pyarrow.parquet as pq

def process_parquet_file(file_path, text_column, chunksize=200):
    chunks = []
    parquet_file = pq.ParquetFile(file_path)
    for batch in parquet_file.iter_batches(batch_size=chunksize):
        chunk = batch.to_pandas()
        chunk['complete_text'] = chunk[text_column].str.lower()
        chunk['complete_text'] = chunk['complete_text'].str.replace('[^\w\s]', '', regex=True)
        chunks.append(chunk)
    return chunks

def sample_long_texts(chunks):
    sampled_chunks = []
    for chunk in chunks:
        sampled_chunk = chunk[chunk['text'].str.len() > 1000].sample(n=10, replace=True)
        sampled_chunks.append(sampled_chunk)
    return sampled_chunks

def warn_memory_usage():
    mem_usage = psutil.virtual_memory().percent
    if mem_usage > 20:
        print(f"Warning: Memory usage is high ({mem_usage}%)")


def tfidf_representation(documents, vectorizer=None):
    processed_documents = (doc for doc in documents if doc.strip() != "")
    if not processed_documents:
        print("No valid documents to vectorize.")
        return None

    if vectorizer is None:
        vectorizer = TfidfVectorizer(max_features=2**16, dtype=np.float32)
        tfidf_matrix = vectorizer.fit_transform(processed_documents)
    else:
        tfidf_matrix = vectorizer.transform(processed_documents)
    
    return tfidf_matrix, vectorizer

@delayed
def process_chunk(chunk_docs, vectorizer, chunk_size):
    embeddings, _ = tfidf_representation(chunk_docs, vectorizer)
    embeddings = embeddings.toarray()  # Convert sparse matrix to dense matrix
    similarity_matrix = pairwise_distances(embeddings, metric='jaccard', n_jobs=-1)
    return da.from_array(similarity_matrix, chunks=(chunk_size, chunk_size))



def minhash_deduplication(chunks, threshold, file_id_column, output_csv, chunk_size=200):
    start_time = time.time()
    warn_memory_usage()
    print("Sampled DataFrame shape:", len(chunks))

    vectorizer = TfidfVectorizer(max_features=2**16, dtype=np.float32)

    with open(output_csv, 'w') as f:
        f.write("file_id1,file_id2,similarity_score\n")

    for chunk in chunks:
        documents = chunk['text'].tolist()
        vectorizer.fit(documents)

        delayed_results = []
        for i in range(0, len(documents), chunk_size):
            chunk_docs = documents[i:i+chunk_size]
            delayed_results.append(process_chunk(chunk_docs, vectorizer, chunk_size))

        similarity_matrices = compute(*delayed_results)

        for similarity_matrix in similarity_matrices:
            similarity_matrix = similarity_matrix.compute_chunk_sizes()  
            duplicates = np.argwhere((similarity_matrix > threshold) & (np.arange(similarity_matrix.shape[0])[:, None] != np.arange(similarity_matrix.shape[0])))
            duplicates = duplicates.compute()  # Compute the duplicates array
            print("Number of duplicates found:", duplicates.shape[0])  

            file_ids = chunk[file_id_column].tolist()[i:i+chunk_size]

            for pair in duplicates:
                file_id1 = file_ids[pair[0]]
                file_id2 = file_ids[pair[1]]
                similarity_score = similarity_matrix[pair[0], pair[1]].compute()
                with open(output_csv, 'a') as f:
                    f.write(f"{file_id1},{file_id2},{similarity_score}\n")

    end_time = time.time()
    duration = end_time - start_time
    print(f"Deduplication process took {duration} seconds.")
    return duration

parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/Czech-PD"
output_csv_separate = "/gpfsdswork/projects/rech/fmr/uft12cr/Czech-PD_deduplication.csv"
files_fr = [f for f in os.listdir(parquet_dir_fr) if f.endswith(".parquet")]



start_time_fr = time.time()
for file_name_fr in files_fr:
    file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
    chunks = process_parquet_file(file_path_fr, 'text', chunksize=200)
    sampled_chunks = sample_long_texts(chunks)

    minhash_deduplication(sampled_chunks, threshold=0.8, file_id_column='identifier', output_csv=output_csv_separate, chunk_size=200)

end_time_fr = time.time()
duration_fr = end_time_fr - start_time_fr
print(f"Deduplication process for Danish-PD took {duration_fr} seconds.")


