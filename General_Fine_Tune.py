import json
import torch
from transformers import BertTokenizer, BertModel, BertConfig, BertForSequenceClassification
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import re
import pandas as pd
import torch.cuda.amp as amp
from torch.optim import AdamW
import numpy as np

import os
os.environ['CUDA_LAUNCH_BLOCKING'] = '1'

import torch
torch.backends.cuda.matmul.allow_tf32 = True
torch.backends.cudnn.allow_tf32 = True
# Paths to locally stored model and tokenizer
MODEL_PATH = '/linkhome/rech/genrug01/uft12cr/bert_Model'
TOKENIZER_PATH = '/linkhome/rech/genrug01/uft12cr/Bert_tokenizer'
General_TOKENIZER_PATH = '/gpfswork/rech/fmr/uft12cr/finetuneAli/vocab2.txt'

# Preprocessing function
def preprocess_text(text):
    text = text.lower()
    text = re.sub(r'[^a-zA-Z\s]', '', text)
    text = re.sub(r'\s+', ' ', text).strip()
    return text

# Load data
decoder = json.JSONDecoder()
data = []
with open('/linkhome/rech/genrug01/uft12cr/arxiv-metadata-oai-snapshot.json') as f:
    for line in f:
        data.append(decoder.decode(line))

# Convert to DataFrame
df = pd.DataFrame(data)

# Preprocess abstracts
df['preprocessed_abstract'] = df['abstract'].apply(preprocess_text)

# Define domain-wise categories
domain_wise_categories = {
    "Mathematics": ["math.AG", "math.AT", "math.AP", "math.CT", "math.CA", "math.CO", "math.AC", "math.CV", "math.DG", "math.DS", "math.FA", "math.GM", "math.GN", "math.GT", "math.GR", "math.HO", "math.IT", "math.KT", "math.LO", "math.MP", "math.MG", "math.NT", "math.NA", "math.OA", "math.OC", "math.PR", "math.QA", "math.RT", "math.RA", "math.SP", "math.ST", "math.SG"],
    "Computer Science": ["cs.AI", "cs.CL", "cs.CC", "cs.CE", "cs.CG", "cs.GT", "cs.CV", "cs.CY", "cs.CR", "cs.DS", "cs.DB", "cs.DL", "cs.DM", "cs.DC", "cs.ET", "cs.FL", "cs.GL", "cs.GR", "cs.AR", "cs.HC", "cs.IR", "cs.IT", "cs.LO", "cs.LG", "cs.MS", "cs.MA", "cs.MM", "cs.NI", "cs.NE", "cs.NA", "cs.OS", "cs.OH", "cs.PF", "cs.PL", "cs.RO", "cs.SI", "cs.SE", "cs.SD", "cs.SC", "cs.SY"],
    "Physics": ["physics.acc-ph", "physics.app-ph", "physics.ao-ph", "physics.atom-ph", "physics.atm-clus", "physics.bio-ph", "physics.chem-ph", "physics.class-ph", "physics.comp-ph", "physics.data-an", "physics.flu-dyn", "physics.gen-ph", "physics.geo-ph", "physics.hist-ph", "physics.ins-ph", "physics.med-ph", "physics.optics", "physics.ed-ph", "physics.soc-ph", "physics.plasm-ph", "physics.pop-ph", "physics.space-ph"],
    "Chemistry": ["nlin.AO", "nlin.CG", "nlin.CD", "nlin.SI", "nlin.PS"],
    "Statistics": ["stat.AP", "stat.CO", "stat.ML", "stat.ME", "stat.OT", "stat.TH"],
    "Biology": ["q-bio.BM", "q-bio.CB", "q-bio.GN", "q-bio.MN", "q-bio.NC", "q-bio.OT", "q-bio.PE", "q-bio.QM", "q-bio.SC", "q-bio.TO"]
}

# Assign domain to each paper
def assign_domain(categories):
    for domain, domain_categories in domain_wise_categories.items():
        if any(cat in categories for cat in domain_categories):
            return domain
    return "Other"

df['domain'] = df['categories'].apply(assign_domain)

print("\nChecking label assignment:")
for domain, categories in domain_wise_categories.items():
    sample = df[df['domain'] == domain].sample(1)
    print(f"\nDomain: {domain}")
    print(f"Categories: {sample['categories'].values[0]}")
    print(f"Abstract: {sample['preprocessed_abstract'].values[0][:100]}...") 

    
# Perform clustering for each domain
domain_clusters = {}
for domain in domain_wise_categories.keys():
    domain_abstracts = df[df['domain'] == domain]['preprocessed_abstract'].tolist()
    if domain_abstracts:
        vectorizer = TfidfVectorizer()
        abstract_vectors = vectorizer.fit_transform(domain_abstracts)

        num_clusters = len(domain_wise_categories[domain])
        kmeans = KMeans(n_clusters=num_clusters)
        clusters = kmeans.fit_predict(abstract_vectors)

        domain_clusters[domain] = [domain_abstracts[i] for i in range(len(domain_abstracts))]

# Prepare data for fine-tuning
all_abstracts = [abstract for abstracts in domain_clusters.values() for abstract in abstracts]
all_labels = [domain for domain, abstracts in domain_clusters.items() for _ in abstracts]

# Load custom tokenizers
try:
    all_cluster_tokenizer = BertTokenizer.from_pretrained('/gpfswork/rech/fmr/uft12cr/finetuneAli/ALL_clusters_vocab2.txt', local_files_only=True)
    final_tokenizer = BertTokenizer.from_pretrained('/gpfswork/rech/fmr/uft12cr/finetuneAli/final_vocab_cluster_pref2.txt', local_files_only=True)
except OSError:
    print("Failed to load tokenizers from txt files. Trying to load from directories.")
    

# Load the general BERT tokenizer
try:
    general_tokenizer = BertTokenizer.from_pretrained(General_TOKENIZER_PATH, do_lower_case=True, local_files_only=True)
except OSError:
    print("Failed to load general BERT tokenizer. Make sure the path is correct and contains the necessary files.")
    raise

# Prepare labels
le = LabelEncoder()
labels = le.fit_transform(all_labels)
num_labels = len(le.classes_)

X_train, X_test, y_train, y_test = train_test_split(all_abstracts, labels, test_size=0.2, random_state=42, stratify=labels)

class ArXivDataset(Dataset):
    def __init__(self, texts, labels, tokenizer, max_length=256):
        self.texts = texts
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_length = max_length

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        text = self.texts[idx]
        label = self.labels[idx]

        encoding = self.tokenizer.encode_plus(
            text,
            add_special_tokens=True,
            max_length=self.max_length,
            return_token_type_ids=False,
            padding='max_length',
            truncation=True,
            return_attention_mask=True,
            return_tensors='pt',
        )

        return {
            'input_ids': encoding['input_ids'].flatten(),
            'attention_mask': encoding['attention_mask'].flatten(),
            'labels': torch.tensor(label, dtype=torch.long)
        }

def create_data_loader(X, y, tokenizer, batch_size=16):
    dataset = ArXivDataset(X, y, tokenizer)
    return DataLoader(dataset, batch_size=batch_size, shuffle=True,drop_last=True)

def setup_model(num_labels, tokenizer):
    config = BertConfig(
        vocab_size=len(tokenizer),
        hidden_size=768,
        num_hidden_layers=12,
        num_attention_heads=12,
        intermediate_size=3072,
        hidden_act="gelu",
        hidden_dropout_prob=0.1,
        attention_probs_dropout_prob=0.1,
        max_position_embeddings=512,
        type_vocab_size=2,
        initializer_range=0.02,
        layer_norm_eps=1e-12,
        pad_token_id=0,
        position_embedding_type="absolute",
        use_cache=True,
        classifier_dropout=None,
        model_type="bert",
        use_gradient_checkpointing=True,
        num_labels=num_labels
    )
    model = BertForSequenceClassification(config)
    try:
        pretrained_model = BertModel.from_pretrained(MODEL_PATH)
        model.bert = pretrained_model.bert
        print(f"Loaded pretrained weights from {MODEL_PATH}")
    except Exception as e:
        print(f"Could not load pretrained weights from {MODEL_PATH}. Starting with random weights. Error: {e}")
    
    return model

def inspect_tokenizer(tokenizer, name):
    print(f"\nInspecting {name} Tokenizer:")
    print(f"Vocab size: {len(tokenizer)}")
    print("Sample from vocabulary:")
    for i in range(10):  # Print first 10 tokens
        print(tokenizer.convert_ids_to_tokens([i]))
    
    # Test with a domain-specific sample
    domain_sample = "The Riemann hypothesis states that the non-trivial zeros of the zeta function have real part 1/2"
    tokens = tokenizer.tokenize(domain_sample)
    print(f"\nTokenization of domain-specific sample:")
    print(tokens)
    
    # Check if the tokenizer can handle pre-tokenized input
    pre_tokenized = ['the', 'riemann', 'hypothesis']
    encoded = tokenizer.convert_tokens_to_ids(pre_tokenized)
    print(f"\nPre-tokenized input:")
    print(f"Input: {pre_tokenized}")
    print(f"Encoded: {encoded}")



def train_model(model, train_loader, val_loader, epochs=3, learning_rate=5e-6):
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model.to(device)

    optimizer = AdamW(model.parameters(), lr=learning_rate)
    scaler = amp.GradScaler()

    for epoch in range(epochs):
        model.train()
        for batch in train_loader:
            input_ids = torch.clamp(batch['input_ids'], max=model.config.vocab_size - 1).to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)

            optimizer.zero_grad()

            try:
                with amp.autocast():
                    outputs = model(input_ids, attention_mask=attention_mask, labels=labels)
                    loss = outputs.loss

                scaler.scale(loss).backward()
                scaler.step(optimizer)
                scaler.update()

            except RuntimeError as e:
                print(f"Error in forward/backward pass: {e}")
                continue

        # Validation
        model.eval()
        val_preds = []
        val_true = []
        with torch.no_grad():
            for batch in val_loader:
                input_ids = torch.clamp(batch['input_ids'], max=model.config.vocab_size - 1).to(device)
                attention_mask = batch['attention_mask'].to(device)
                labels = batch['labels'].to(device)

                try:
                    outputs = model(input_ids, attention_mask=attention_mask)
                    val_preds.extend(outputs.logits.argmax(dim=1).cpu().numpy())
                    val_true.extend(labels.cpu().numpy())
                except RuntimeError as e:
                    print(f"Error in validation: {e}")
                    continue

        val_accuracy = accuracy_score(val_true, val_preds)
        val_f1 = f1_score(val_true, val_preds, average='weighted')

        print(f'Epoch {epoch+1}/{epochs}:')
        print(f'Val Accuracy: {val_accuracy:.4f}, Val F1: {val_f1:.4f}')

    return model

print("\nVerifying tokenizer differences:")
sample_text = "This is a sample text for tokenization comparison."
# Train and evaluate models with different tokenizers
tokenizers = [all_cluster_tokenizer, final_tokenizer, general_tokenizer]
tokenizer_names = ['All Cluster', 'Final', 'General']
# Inspect each tokenizer
for tokenizer, name in zip(tokenizers, tokenizer_names):
    inspect_tokenizer(tokenizer, name)
    
for tokenizer, name in zip(tokenizers, tokenizer_names):
    tokens = tokenizer.tokenize(sample_text)
    print(f"\n{name} Tokenizer:")
    print(f"Tokens: {tokens}")
    print(f"Vocab size: {len(tokenizer)}")


for tokenizer, name in zip(tokenizers, tokenizer_names):
    print(f"\nTraining with {name} tokenizer:")
    
    train_loader = create_data_loader(X_train, y_train, tokenizer)
    val_loader = create_data_loader(X_test, y_test, tokenizer)
    
    model = setup_model(num_labels, tokenizer)
    trained_model = train_model(model, train_loader, val_loader)
    
    # Evaluate the model
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    model.eval()
    test_preds = []
    test_true = []
    with torch.no_grad():
        for batch in val_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)

            outputs = model(input_ids, attention_mask=attention_mask)
            test_preds.extend(outputs.logits.cpu().numpy())
            test_true.extend(labels.cpu().numpy())

    test_preds = np.array(test_preds)
    test_true = np.array(test_true)

    # One-hot encode the true labels
    test_true_one_hot = np.eye(num_labels)[test_true]

    test_accuracy = accuracy_score(test_true, test_preds.argmax(axis=1))
    test_f1 = f1_score(test_true, test_preds.argmax(axis=1), average='weighted')
    test_auc = roc_auc_score(test_true_one_hot, test_preds, multi_class='ovr')

    print(f"\nTest Results for {name} tokenizer:")
    print(f"Accuracy: {test_accuracy:.4f}")
    print(f"F1 Score: {test_f1:.4f}")
    print(f"AUC-ROC: {test_auc:.4f}")

# Print class distribution
unique, counts = np.unique(y_train, return_counts=True)
print("\nClass distribution in training set:")
for u, c in zip(unique, counts):
    print(f"Class {le.inverse_transform([u])[0]}: {c} samples")