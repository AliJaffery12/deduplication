import pandas as pd
import os
import numpy as np
from sklearn.feature_extraction.text import HashingVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import time
import psutil
from dask import delayed, compute

def process_parquet_file(file_path, text_column):
    parquet_df = pd.read_parquet(file_path)
    parquet_df['complete_text'] = parquet_df[text_column].str.lower()
    parquet_df['complete_text'] = parquet_df['complete_text'].str.replace('[^\w\s]', '', regex=True)
    return parquet_df

def sample_long_texts(parquet_df):
    sampled_df = parquet_df[parquet_df['complete_text'].str.len() > 1000].sample(n=10, replace=True)
    return sampled_df

def warn_memory_usage():
    mem_usage = psutil.virtual_memory().percent
    if mem_usage > 20:
        print(f"Warning: Memory usage is high ({mem_usage}%)")

def tfidf_representation(documents, vectorizer=None):
    processed_documents = (doc for doc in documents if doc.strip() != "")
    if not processed_documents:
        print("No valid documents to vectorize.")
        return None

    if vectorizer is None:
        vectorizer = HashingVectorizer(n_features=2**16, dtype=np.float32)
        tfidf_matrix = vectorizer.fit_transform(processed_documents)
    else:
        tfidf_matrix = vectorizer.transform(processed_documents)
    
    return tfidf_matrix, vectorizer

#def append_duration_to_csv(output_csv, process_name, duration):
 #   df = pd.DataFrame([[process_name, duration]], columns=['process_name', 'duration'])
  #  if not os.path.isfile(output_csv):
   #     df.to_csv(output_csv, index=False)
    #else:
     #   df.to_csv(output_csv, mode='a', header=False, index=False)

@delayed
def process_chunk(chunk_docs, vectorizer):
    embeddings, _ = tfidf_representation(chunk_docs, vectorizer)
    similarity_matrix = cosine_similarity(embeddings)
    return similarity_matrix

def minhash_deduplication(sample_df, threshold, file_id_column, output_csv, chunk_size=20):
    start_time = time.time()
    warn_memory_usage()
    print("Sampled DataFrame shape:", sample_df.shape)

    documents = sample_df['complete_text'].tolist()
    num_docs = len(documents)

    vectorizer = HashingVectorizer(n_features=2**16, dtype=np.float32)
    vectorizer.fit(documents)

    with open(output_csv, 'w') as f:
        f.write("file_id1,file_id2,similarity_score\n")

    for i in range(0, num_docs, chunk_size):
        chunk_docs = documents[i:i+chunk_size]
        similarity_matrix = process_chunk(chunk_docs, vectorizer).compute()
        print("Similarity matrix shape:", similarity_matrix.shape)

        duplicates = np.argwhere((similarity_matrix > threshold) & (np.arange(len(similarity_matrix))[:, None] != np.arange(len(similarity_matrix))))
        print("Number of duplicates found:", len(duplicates))

        file_ids = sample_df[file_id_column].tolist()[i:i+chunk_size]

        for pair in duplicates:
            file_id1 = file_ids[pair[0]]
            file_id2 = file_ids[pair[1]]
            similarity_score = similarity_matrix[pair[0], pair[1]]
            with open(output_csv, 'a') as f:
                f.write(f"{file_id1},{file_id2},{similarity_score}\n")

    end_time = time.time()
    duration = end_time - start_time
    print(f"Deduplication process took {duration} seconds.")
    return duration

parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books"
output_csv_separate = "/gpfsdswork/projects/rech/fmr/uft12cr/deduplication_results_frenchPD_CompleteDataset_IDs.csv"
files_fr = [f for f in os.listdir(parquet_dir_fr) if f.endswith(".parquet")]

start_time_fr = time.time()
for file_name_fr in files_fr:
    file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
    parquet_df_fr = process_parquet_file(file_path_fr, 'complete_text')
    sampled_df_fr = sample_long_texts(parquet_df_fr)

    minhash_deduplication(sampled_df_fr, threshold=0.8, file_id_column='file_id', output_csv=output_csv_separate)

end_time_fr = time.time()
duration_fr = end_time_fr - start_time_fr
print(f"Deduplication process for French-PD-Books took {duration_fr} seconds.")
#append_duration_to_csv(output_csv_separate, 'French-PD-Books Deduplication', duration_fr)
