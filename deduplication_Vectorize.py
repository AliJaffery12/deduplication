import subprocess
import resource
import pandas as pd
import os
import numpy as np
from scipy.spatial.transform import Rotation as R
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import time
import psutil

def process_parquet_file(file_path, text_column):
    parquet_df = pd.read_parquet(file_path)
    parquet_df['complete_text'] = parquet_df[text_column].str.lower()
    parquet_df['complete_text'] = parquet_df['complete_text'].str.replace('[^\w\s]', '', regex=True)
    return parquet_df

def sample_long_texts(parquet_df):
    sampled_df = parquet_df[parquet_df['complete_text'].str.len() > 1000].sample(n=10, replace=True)
    return sampled_df

def tfidf_representation(documents):
    # Remove documents that are empty or contain only stop words
    processed_documents = [doc for doc in documents if doc.strip() != ""]
    if not processed_documents:
        print("No valid documents to vectorize.")
        return None

    vectorizer = TfidfVectorizer()
    tfidf_matrix = vectorizer.fit_transform(processed_documents)
    return tfidf_matrix


def warn_memory_usage():
    mem_usage = psutil.virtual_memory().percent
    if mem_usage > 20:
        print(f"Warning: Memory usage is high ({mem_usage}%)")

def minhash_deduplication(sample_df, threshold, file_id_column, title_column, output_csv):
    
    warn_memory_usage()
    print("Sampled DataFrame shape:", sample_df.shape)
    documents = sample_df['complete_text'].tolist()
    tfidf_matrix = tfidf_representation(documents)

    if tfidf_matrix is None:
        # No valid documents to process
        print("No valid documents found for deduplication.")
        return

    similarity_matrix = cosine_similarity(tfidf_matrix)
    print("Similarity matrix shape:", similarity_matrix.shape)

    duplicates = [(i, j) for i in range(len(similarity_matrix)) for j in range(i+1, len(similarity_matrix)) if similarity_matrix[i][j] > threshold]
    print("Number of duplicates found:", len(duplicates))

    file_ids = sample_df[file_id_column].tolist()
    titles = sample_df[title_column].tolist()

    ranked_duplicates = []
    for i, j in duplicates:
        file_id1 = file_ids[i]
        file_id2 = file_ids[j]
        title1 = titles[i]
        title2 = titles[j]
        similarity_score = similarity_matrix[i][j]
        ranked_duplicates.append((file_id1, file_id2, similarity_score, title1, title2))

    if ranked_duplicates:
        df = pd.DataFrame(ranked_duplicates, columns=['file_id1', 'file_id2', 'similarity_score', 'title1', 'title2'])
        df.to_csv(output_csv, index=False)
        print(f"Deduplication results saved to {output_csv}")
    else:
        print("No duplicates found.")

    for file_id1, file_id2, similarity_score, title1, title2 in ranked_duplicates:
        print(f"Files '{file_id1}' and '{file_id2}' are similar with score {similarity_score}. Metadata: {title1}, {title2}")

    



    

def rotation_alignment(embeddings_fr, embeddings_diverse):
    covariance_matrix = np.dot(embeddings_fr.T, embeddings_diverse)
    U, _, Vt = np.linalg.svd(covariance_matrix)
    rotation_matrix = np.dot(U, Vt)
    aligned_embeddings_diverse = np.dot(embeddings_diverse, rotation_matrix.T)
    return aligned_embeddings_diverse

thresholds = [0.8]

# Directories with the full dataset
parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books"
parquet_dir_diverse = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-diverse"

output_csv_separate = "/gpfsdswork/projects/rech/fmr/uft12cr/deduplication_results_frenchPD_complete_Dataset.csv"
output_csv_separate_it = "/gpfsdswork/projects/rech/fmr/uft12cr/deduplication_results_Frech_Diverse_complete_Dataset.csv"
output_csv_cross_lingual = "/gpfsdswork/projects/rech/fmr/uft12cr/cross_lingual_deduplication_PD_Diverse_complete_Dataset.csv"

def process_dataset_in_batches(parquet_dir, text_column, batch_size):
    files = [f for f in os.listdir(parquet_dir) if f.endswith(".parquet")][:3]
    for file_name in files:
        file_path = os.path.join(parquet_dir, file_name)
        parquet_df = process_parquet_file(file_path, text_column)
        num_batches = len(parquet_df) // batch_size + 1
        for batch_index in range(num_batches):
            start_index = batch_index * batch_size
            end_index = min((batch_index + 1) * batch_size, len(parquet_df))
            batch_df = parquet_df[start_index:end_index]
            yield batch_df

# Define batch size
batch_size = 100

start_time_fr = time.time()
# Process batches for French-PD-Books
for batch_df_fr in process_dataset_in_batches(parquet_dir_fr, 'complete_text', batch_size):
    for threshold in thresholds:
        warn_memory_usage()
        minhash_deduplication(batch_df_fr, threshold, file_id_column='file_id', title_column='title', output_csv=output_csv_separate)

end_time_fr = time.time()
duration_fr = end_time_fr - start_time_fr
print(f"Deduplication process for French-PD-Books took {duration_fr} seconds.")


start_time_diverse = time.time()

# Process batches for French-PD-diverse
for batch_df_diverse in process_dataset_in_batches(parquet_dir_diverse, 'complete_text', batch_size):
    for threshold in thresholds:
        warn_memory_usage()
        minhash_deduplication(batch_df_diverse, threshold, file_id_column='identifier', title_column='title', output_csv=output_csv_separate_it)
end_time_diverse = time.time()
duration_diverse = end_time_diverse - start_time_diverse
print(f"Deduplication process for French-PD-diverse took {duration_diverse} seconds.")

def cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, threshold=0.5, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='file_id', title_column_diverse='title', output_csv=output_csv_cross_lingual, batch_size=100):
    common_documents = []
    
    
    # Create a TF-IDF vectorizer with a shared vocabulary
    vectorizer = TfidfVectorizer()

    # Get the list of files for French-PD-Books and French-PD-diverse
    files_fr = [f for f in os.listdir(parquet_dir_fr) if f.endswith(".parquet")][:3]
    files_diverse = [f for f in os.listdir(parquet_dir_diverse) if f.endswith(".parquet")][:3]

    # Process batches for French-PD-Books
    for batch_df_fr in process_dataset_in_batches(parquet_dir_fr, 'complete_text', batch_size):
        # Process batches for French-PD-diverse
        for batch_df_diverse in process_dataset_in_batches(parquet_dir_diverse, 'complete_text', batch_size):
            documents_fr = batch_df_fr['complete_text'].tolist()
            documents_diverse = batch_df_diverse['complete_text'].tolist()

            # Fit the vectorizer on both sets of documents to learn the vocabulary
            vectorizer.fit(documents_fr + documents_diverse)

            # Transform documents into TF-IDF matrices using the shared vocabulary
            tfidf_matrix_fr = vectorizer.transform(documents_fr)
            tfidf_matrix_diverse = vectorizer.transform(documents_diverse)

            # Compute cosine similarity between the TF-IDF matrices
            similarity_matrix = cosine_similarity(tfidf_matrix_fr, tfidf_matrix_diverse)

            # Find pairs of documents with similarity above the threshold
            common_indices = [(i, j) for i in range(len(similarity_matrix)) for j in range(len(similarity_matrix[0])) if similarity_matrix[i][j] > threshold]

            # Extract file IDs and titles for the common documents
            file_ids_fr = batch_df_fr[file_id_column_fr].tolist()
            titles_fr = batch_df_fr[title_column_fr].tolist()
            file_ids_diverse = batch_df_diverse[file_id_column_diverse].tolist()
            titles_diverse = batch_df_diverse[title_column_diverse].tolist()

            # Store information about common documents
            for idx_fr, idx_diverse in common_indices:
                file_id_fr = file_ids_fr[idx_fr]
                title_fr = titles_fr[idx_fr]
                file_id_diverse = file_ids_diverse[idx_diverse]
                title_diverse = titles_diverse[idx_diverse]
                similarity_score = similarity_matrix[idx_fr][idx_diverse]
                common_documents.append((file_id_fr, title_fr, file_id_diverse, title_diverse, similarity_score))

    # Write common documents to CSV
    df = pd.DataFrame(common_documents, columns=['file_id_fr', 'title_fr', 'file_id_diverse', 'title_diverse', 'similarity_score'])
    df.to_csv(output_csv, index=False)
    
    # Print information about common documents
    print("Common French_PD/French_PD Diverse documents:")
    for file_id_fr, title_fr, file_id_diverse, title_diverse, similarity_score in common_documents:
        print(f"French_PD document '{file_id_fr}' ({title_fr}) is similar to French_PD_Diverse document '{file_id_diverse}' ({title_diverse}) with similarity score {similarity_score}")
    
    # Calculate duration and print
    

# Define batch size for cross-lingual similarity calculation
cross_lingual_batch_size = 100
cross_lingual_start_time = time.time()
cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, threshold=0.8, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='identifier', title_column_diverse='title', output_csv=output_csv_cross_lingual, batch_size=cross_lingual_batch_size)

cross_lingual_end_time = time.time()
cross_lingual_total_duration = cross_lingual_end_time - cross_lingual_start_time
print(f"Total cross-lingual similarity process took {cross_lingual_total_duration} seconds.")
