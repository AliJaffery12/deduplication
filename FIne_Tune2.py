import json
import torch
from transformers import BertTokenizer, BertModel, BertConfig
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
import re
import pandas as pd
import torch.cuda.amp as amp

# Paths to locally stored model and tokenizer
MODEL_PATH = '/linkhome/rech/genrug01/uft12cr/bert_Model'
TOKENIZER_PATH = '/linkhome/rech/genrug01/uft12cr/Bert_tokenizer'

# Preprocessing function
def preprocess_text(text):
    text = text.lower()
    text = re.sub(r'[^a-zA-Z\s]', '', text)
    text = re.sub(r'\s+', ' ', text).strip()
    return text

# Load data
decoder = json.JSONDecoder()
data = []
with open('/linkhome/rech/genrug01/uft12cr/arxiv-metadata-oai-snapshot.json') as f:
    for line in f:
        data.append(decoder.decode(line))

# Convert to DataFrame
df = pd.DataFrame(data)

# Preprocess abstracts
df['preprocessed_abstract'] = df['abstract'].apply(preprocess_text)

# Define domain-wise categories
domain_wise_categories = {
    "Mathematics": ["math.AG", "math.AT", "math.AP", "math.CT", "math.CA", "math.CO", "math.AC", "math.CV", "math.DG", "math.DS", "math.FA", "math.GM", "math.GN", "math.GT", "math.GR", "math.HO", "math.IT", "math.KT", "math.LO", "math.MP", "math.MG", "math.NT", "math.NA", "math.OA", "math.OC", "math.PR", "math.QA", "math.RT", "math.RA", "math.SP", "math.ST", "math.SG"],
    "Computer Science": ["cs.AI", "cs.CL", "cs.CC", "cs.CE", "cs.CG", "cs.GT", "cs.CV", "cs.CY", "cs.CR", "cs.DS", "cs.DB", "cs.DL", "cs.DM", "cs.DC", "cs.ET", "cs.FL", "cs.GL", "cs.GR", "cs.AR", "cs.HC", "cs.IR", "cs.IT", "cs.LO", "cs.LG", "cs.MS", "cs.MA", "cs.MM", "cs.NI", "cs.NE", "cs.NA", "cs.OS", "cs.OH", "cs.PF", "cs.PL", "cs.RO", "cs.SI", "cs.SE", "cs.SD", "cs.SC", "cs.SY"],
    "Physics": ["physics.acc-ph", "physics.app-ph", "physics.ao-ph", "physics.atom-ph", "physics.atm-clus", "physics.bio-ph", "physics.chem-ph", "physics.class-ph", "physics.comp-ph", "physics.data-an", "physics.flu-dyn", "physics.gen-ph", "physics.geo-ph", "physics.hist-ph", "physics.ins-ph", "physics.med-ph", "physics.optics", "physics.ed-ph", "physics.soc-ph", "physics.plasm-ph", "physics.pop-ph", "physics.space-ph"],
    "Chemistry": ["nlin.AO", "nlin.CG", "nlin.CD", "nlin.SI", "nlin.PS"],
    "Statistics": ["stat.AP", "stat.CO", "stat.ML", "stat.ME", "stat.OT", "stat.TH"],
    "Biology": ["q-bio.BM", "q-bio.CB", "q-bio.GN", "q-bio.MN", "q-bio.NC", "q-bio.OT", "q-bio.PE", "q-bio.QM", "q-bio.SC", "q-bio.TO"]
}

# Assign domain to each paper
def assign_domain(categories):
    for domain, domain_categories in domain_wise_categories.items():
        if any(cat in categories for cat in domain_categories):
            return domain
    return "Other"

df['domain'] = df['categories'].apply(assign_domain)

# Perform clustering for each domain
domain_clusters = {}
for domain in domain_wise_categories.keys():
    domain_abstracts = df[df['domain'] == domain]['preprocessed_abstract'].tolist()
    if domain_abstracts:
        vectorizer = TfidfVectorizer()
        abstract_vectors = vectorizer.fit_transform(domain_abstracts)

        num_clusters = len(domain_wise_categories[domain])
        kmeans = KMeans(n_clusters=num_clusters)
        clusters = kmeans.fit_predict(abstract_vectors)

        domain_clusters[domain] = [domain_abstracts[i] for i in range(len(domain_abstracts))]

# Prepare data for fine-tuning
all_abstracts = [abstract for abstracts in domain_clusters.values() for abstract in abstracts]
all_labels = [domain for domain, abstracts in domain_clusters.items() for _ in abstracts]

# Load custom tokenizers
try:
    all_cluster_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/ALL_clusters_vocab.txt', local_files_only=True)
    final_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/final_vocab_cluster_pref.txt', local_files_only=True)
except OSError:
    print("Failed to load tokenizers from txt files. Trying to load from directories.")
    all_cluster_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/ALL_clusters_vocab', local_files_only=True)
    final_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/final_vocab_cluster_pref', local_files_only=True)

# Create a mapping from labels to IDs
label_to_id = {label: i for i, label in enumerate(set(all_labels))}

def tokenize_data(abstracts, labels, tokenizer):
    tokenized_data = []
    for abstract, label in zip(abstracts, labels):
        inputs = tokenizer.encode_plus(
            abstract,
            add_special_tokens=True,
            max_length=512,
            return_attention_mask=True,
            return_tensors='pt',
            padding='max_length',
            truncation=True
        )
        tokenized_data.append({
            'input_ids': inputs['input_ids'].flatten(),
            'attention_mask': inputs['attention_mask'].flatten(),
            'label': label_to_id[label]
        })
    return tokenized_data

all_cluster_tokenized_data = tokenize_data(all_abstracts, all_labels, all_cluster_tokenizer)
final_tokenized_data = tokenize_data(all_abstracts, all_labels, final_tokenizer)

class ArxivDataset(Dataset):
    def __init__(self, tokenized_data):
        self.tokenized_data = tokenized_data

    def __len__(self):
        return len(self.tokenized_data)

    def __getitem__(self, idx):
        item = self.tokenized_data[idx]
        return {
            'input_ids': item['input_ids'],
            'attention_mask': item['attention_mask'],
            'label': item['label']
        }

all_cluster_dataset = ArxivDataset(all_cluster_tokenized_data)
final_dataset = ArxivDataset(final_tokenized_data)

all_cluster_data_loader = DataLoader(all_cluster_dataset, batch_size=8, shuffle=True)
final_data_loader = DataLoader(final_dataset, batch_size=8, shuffle=True)

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def fine_tune_model(data_loader, tokenizer):
    config = BertConfig(
        vocab_size=30522,
        hidden_size=768,
        num_hidden_layers=12,
        num_attention_heads=12,
        intermediate_size=3072,
        hidden_act="gelu",
        hidden_dropout_prob=0.1,
        attention_probs_dropout_prob=0.1,
        max_position_embeddings=512,
        type_vocab_size=2,
        initializer_range=0.02,
        layer_norm_eps=1e-12,
        pad_token_id=0,
        position_embedding_type="absolute",
        use_cache=True,
        classifier_dropout=None,
        model_type="bert",
        use_gradient_checkpointing=True,
    )
    model = BertModel(config)
    model = model.to(device)
    classification_head = torch.nn.Linear(model.config.hidden_size, len(label_to_id)).to(device)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(list(model.parameters()) + list(classification_head.parameters()), lr=1e-5)
    scaler = amp.GradScaler()

    for epoch in range(3):
        model.train()
        total_loss = 0
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['label'].to(device)

            optimizer.zero_grad()

            with amp.autocast():
                outputs = model(input_ids, attention_mask=attention_mask)
                pooled_output = outputs.pooler_output
                logits = classification_head(pooled_output)
                loss = criterion(logits, labels)

            scaler.scale(loss).backward()
            scaler.step(optimizer)
            scaler.update()
            total_loss += loss.item()

        print(f'Epoch {epoch+1}, Loss: {total_loss / len(data_loader)}')

    model.eval()
    return model, classification_head

all_cluster_model, all_cluster_head = fine_tune_model(all_cluster_data_loader, all_cluster_tokenizer)
final_model, final_head = fine_tune_model(final_data_loader, final_tokenizer)

def evaluate_model(model, classification_head, data_loader):
    model.eval()
    predictions = []
    true_labels = []
    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels_batch = batch['label'].to(device)

            outputs = model(input_ids, attention_mask=attention_mask)
            pooled_output = outputs.pooler_output
            logits = classification_head(pooled_output)
            _, predicted = torch.max(logits, 1)

            predictions.extend(predicted.cpu().numpy())
            true_labels.extend(labels_batch.cpu().numpy())

    accuracy = accuracy_score(true_labels, predictions)
    f1 = f1_score(true_labels, predictions, average='macro')

    y_true = torch.nn.functional.one_hot(torch.tensor(true_labels), num_classes=len(label_to_id)).numpy()
    y_pred = torch.nn.functional.one_hot(torch.tensor(predictions), num_classes=len(label_to_id)).numpy()

    auc_roc = roc_auc_score(y_true, y_pred, average='macro', multi_class='ovr')

    return accuracy, f1, auc_roc

all_cluster_accuracy, all_cluster_f1, all_cluster_auc_roc = evaluate_model(all_cluster_model, all_cluster_head, all_cluster_data_loader)
final_accuracy, final_f1, final_auc_roc = evaluate_model(final_model, final_head, final_data_loader)

print('All Cluster Vocab:')
print(f'Accuracy: {all_cluster_accuracy:.4f}')
print(f'F1-score: {all_cluster_f1:.4f}')
print(f'AUC-ROC: {all_cluster_auc_roc:.4f}')

print('Final Vocab:')
print(f'Accuracy: {final_accuracy:.4f}')
print(f'F1-score: {final_f1:.4f}')
print(f'AUC-ROC: {final_auc_roc:.4f}')
