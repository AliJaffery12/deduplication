import os
import pyarrow as pa
import pyarrow.parquet as pq
import pandas as pd
import re
import jupytext
import nbformat
import base64
import json

# Set the paths
input_path = '/gpfswork/rech/fmr/uft12cr/corpus/big_code_1/data/jupyter-notebook'
output_path = '/gpfsscratch/rech/fmr/uft12cr/corpus/code/jupytext'

# Create the output directory if it doesn't exist
os.makedirs(output_path, exist_ok=True)

def is_likely_image_data(s):
    if not isinstance(s, str):
        return False
    return len(s) > 100 and bool(re.match(r'^[A-Za-z0-9+/]+={0,2}$', s))

def process_notebook(content):
    try:
        nb = nbformat.reads(content, as_version=4)
        for cell in nb.cells:
            if cell.cell_type == 'code':
                cell.outputs = [output for output in cell.outputs if 
                                'data' not in output or 
                                'image/png' not in output.data]
        return nbformat.writes(nb)
    except:
        return content

def print_sample(original, processed):
    print("Original:")
    print(json.dumps(json.loads(original), indent=2)[:500] + "...\n")
    print("Processed:")
    print(json.dumps(json.loads(processed), indent=2)[:500] + "...\n")
    print("-" * 80)

samples_printed = 0

for filename in os.listdir(input_path):
    if filename.endswith('.parquet'):
        input_file = os.path.join(input_path, filename)
        output_file = os.path.join(output_path, filename)

        table = pq.read_table(input_file)
        df = table.to_pandas()

        if 'content' in df.columns:
            processed_contents = []
            for i, content in enumerate(df['content']):
                processed = process_notebook(content)
                processed_contents.append(processed)
                
                if samples_printed < 3 and content != processed:
                    print(f"Sample {samples_printed + 1} from {filename}:")
                    print_sample(content, processed)
                    samples_printed += 1

            df['content'] = processed_contents
            df = df[~df['content'].apply(is_likely_image_data)]

        pq.write_table(pa.Table.from_pandas(df), output_file)
        print(f"Processed {filename}")

print("All files processed and saved in", output_path)