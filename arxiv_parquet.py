from datasets import load_dataset
from huggingface_hub import HfApi

# Your Hugging Face token
token = "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"

# Step 1: Load the dataset from Hugging Face
dataset = load_dataset("Alijeff1214/DILA_DATASET", data_files="arxiv-metadata-oai-snapshot.json
", split='train')

# Step 2: Save the dataset to Parquet format
dataset.to_parquet("xarciv.parquet")

# Step 3: Upload the Parquet file back to Hugging Face
api = HfApi()

# Upload the Parquet file
api.upload_file(
    path_or_fileobj="xarciv.parquet",
    path_in_repo="xarciv.parquet",
    repo_id="Alijeff1214/DILA_DATASET",
    repo_type="dataset",
    token=token
)
