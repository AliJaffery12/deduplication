import subprocess
import resource
import pandas as pd
import os
import numpy as np
from scipy.spatial.transform import Rotation as R
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.metrics.pairwise import cosine_similarity
import time
import psutil
import logging


def process_parquet_file(file_path, text_column):
    parquet_df = pd.read_parquet(file_path)
    parquet_df['complete_text'] = parquet_df[text_column].str.lower()
    parquet_df['complete_text'] = parquet_df['complete_text'].str.replace('[^\w\s]', '', regex=True)
    return parquet_df

def sample_long_texts(parquet_df):
    sampled_df = parquet_df[parquet_df['complete_text'].str.len() > 1000].sample(n=10, replace=True)
    return sampled_df

def compute_document_embeddings(documents, model):
    embeddings = model.encode(documents)
    return embeddings

def warn_memory_usage():
    mem_usage = psutil.virtual_memory().percent
    if mem_usage > 20:
        print(f"Warning: Memory usage is high ({mem_usage}%)")

def tfidf_representation(documents, vectorizer=None):
    processed_documents = [doc for doc in documents if doc.strip() != ""]
    if not processed_documents:
        print("No valid documents to vectorize.")
        return None

    if vectorizer is None:
        vectorizer = TfidfVectorizer()
        tfidf_matrix = vectorizer.fit_transform(processed_documents)
    else:
        tfidf_matrix = vectorizer.transform(processed_documents)
    
    return tfidf_matrix, vectorizer
def append_duration_to_csv(output_csv, process_name, duration):
    df = pd.DataFrame([[process_name, duration]], columns=['process_name', 'duration'])
    if not os.path.isfile(output_csv):
        df.to_csv(output_csv, index=False)
    else:
        df.to_csv(output_csv, mode='a', header=False, index=False)

def minhash_deduplication(sample_df, threshold, file_id_column, title_column, output_csv):
    start_time = time.time()
    warn_memory_usage()
    print("Sampled DataFrame shape:", sample_df.shape)
    documents = sample_df['complete_text'].tolist()
    embeddings, _ = tfidf_representation(documents)  # No need to pass vectorizer for separate datasets

    similarity_matrix = cosine_similarity(embeddings)
    print("Similarity matrix shape:", similarity_matrix.shape)

    duplicates = np.argwhere((similarity_matrix > threshold) & (np.arange(len(similarity_matrix))[:, None] != np.arange(len(similarity_matrix))))
    print("Number of duplicates found:", len(duplicates))

    file_ids = sample_df[file_id_column].tolist()
    titles = sample_df[title_column].tolist()

    ranked_duplicates = []
    for pair in duplicates:
        file_id1 = file_ids[pair[0]]
        file_id2 = file_ids[pair[1]]
        title1 = titles[pair[0]]
        title2 = titles[pair[1]]
        similarity_score = similarity_matrix[pair[0], pair[1]]
        ranked_duplicates.append((file_id1, file_id2, similarity_score, title1, title2))

    if ranked_duplicates:
        df = pd.DataFrame(ranked_duplicates, columns=['file_id1', 'file_id2', 'similarity_score', 'title1', 'title2'])
        df.to_csv(output_csv, index=False)
        print(f"Deduplication results saved to {output_csv}")
    else:
        print("No duplicates found.")

    for file_id1, file_id2, similarity_score, title1, title2 in ranked_duplicates:
        print(f"Files '{file_id1}' and '{file_id2}' are similar with score {similarity_score}. Metadata: {title1}, {title2}")

    end_time = time.time()
    duration = end_time - start_time
    print(f"Deduplication process took {duration} seconds.")
    return duration

def rotation_alignment(embeddings_fr, embeddings_diverse):
    covariance_matrix = np.dot(embeddings_fr.T, embeddings_diverse)
    
    # Check if covariance_matrix is empty
    if covariance_matrix.size == 0:
        logging.error("Covariance matrix is empty.")
        return None
    
    # Check if covariance_matrix has unexpected dimensions
    if covariance_matrix.ndim != 2:
        logging.error("Unexpected dimensions for covariance matrix.")
        return None
    
    U, _, Vt = np.linalg.svd(covariance_matrix)
    rotation_matrix = np.dot(U, Vt)
    aligned_embeddings_diverse = np.dot(embeddings_diverse, rotation_matrix.T)
    return aligned_embeddings_diverse

thresholds = [0.8]

#model = SentenceTransformer('xlm-r-100langs-bert-base-nli-mean-tokens',local_files_only=True)

# Directories with the full dataset
parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books"
parquet_dir_diverse = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-diverse"

output_csv_separate = "/gpfsdswork/projects/rech/fmr/uft12cr/deduplication_results_frenchPD.csv"
output_csv_separate_it = "/gpfsdswork/projects/rech/fmr/uft12cr/deduplication_results_Frech_Diverse.csv"
output_csv_cross_lingual = "/gpfsdswork/projects/rech/fmr/uft12cr/cross_lingual_deduplication_PD_Diverse.csv"

# Get the first 5 files from each directory
files_fr = [f for f in os.listdir(parquet_dir_fr) if f.endswith(".parquet")][:3]
files_diverse = [f for f in os.listdir(parquet_dir_diverse) if f.endswith(".parquet")][:3]


start_time_fr = time.time()
# Process first 5 files from French-PD-Books
for file_name_fr in files_fr:
    file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
    parquet_df_fr = process_parquet_file(file_path_fr, 'complete_text')
    sampled_df_fr = sample_long_texts(parquet_df_fr)

    for threshold in thresholds:
        warn_memory_usage()
        minhash_deduplication(sampled_df_fr, threshold, file_id_column='file_id', title_column='title', output_csv=output_csv_separate)
end_time_fr = time.time()
duration_fr = end_time_fr - start_time_fr
print(f"Deduplication process for French-PD-Books took {duration_fr} seconds.")
append_duration_to_csv(output_csv_separate, 'French-PD-Books Deduplication', duration_fr)


start_time_diverse = time.time()
# Process first 5 files from French-PD-diverse
for file_name_diverse in files_diverse:
    file_path_it = os.path.join(parquet_dir_diverse, file_name_diverse)
    parquet_df_it = process_parquet_file(file_path_it, 'complete_text')
    sampled_df_it = sample_long_texts(parquet_df_it)

    for threshold in thresholds:
        warn_memory_usage()
        minhash_deduplication(sampled_df_it, threshold,  file_id_column='identifier', title_column='title', output_csv=output_csv_separate_it)

end_time_diverse = time.time()
duration_diverse = end_time_diverse - start_time_diverse
print(f"Deduplication process for French-PD-diverse took {duration_diverse} seconds.")
append_duration_to_csv(output_csv_separate_it, 'French-PD-diverse Deduplication', duration_diverse)


def cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, threshold=0.5, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='file_id', title_column_diverse='title', output_csv=output_csv_cross_lingual):
    common_documents = []
    start_time = time.time()

    # Get the first 5 files from each directory
    files_fr = [f for f in os.listdir(parquet_dir_fr) if f.endswith(".parquet")][:3]
    files_diverse = [f for f in os.listdir(parquet_dir_diverse) if f.endswith(".parquet")][:3]

    all_documents_fr = []
    all_documents_diverse = []

    for file_name_fr in files_fr:
        file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
        parquet_df_fr = process_parquet_file(file_path_fr, 'complete_text')
        sampled_df_fr = sample_long_texts(parquet_df_fr)
        all_documents_fr.extend(sampled_df_fr['complete_text'].tolist())

    for file_name_diverse in files_diverse:
        file_path_it = os.path.join(parquet_dir_diverse, file_name_diverse)
        parquet_df_it = process_parquet_file(file_path_it, 'complete_text')
        sampled_df_it = sample_long_texts(parquet_df_it)
        all_documents_diverse.extend(sampled_df_it['complete_text'].tolist())

    combined_documents = all_documents_fr + all_documents_diverse
    vectorizer = TfidfVectorizer().fit(combined_documents)

    for file_name_fr in files_fr:
        file_path_fr = os.path.join(parquet_dir_fr, file_name_fr)
        parquet_df_fr = process_parquet_file(file_path_fr, 'complete_text')
        sampled_df_fr = sample_long_texts(parquet_df_fr)
        embeddings_fr, _ = tfidf_representation(sampled_df_fr['complete_text'].tolist(), vectorizer)

        for file_name_diverse in files_diverse:
            file_path_it = os.path.join(parquet_dir_diverse, file_name_diverse)
            parquet_df_it = process_parquet_file(file_path_it, 'complete_text')
            sampled_df_it = sample_long_texts(parquet_df_it)
            embeddings_diverse, _ = tfidf_representation(sampled_df_it['complete_text'].tolist(), vectorizer)

            similarity_matrix = cosine_similarity(embeddings_fr, embeddings_diverse)
            common_indices = np.argwhere(similarity_matrix > threshold)

            file_ids_fr = sampled_df_fr[file_id_column_fr].tolist()
            titles_fr = sampled_df_fr[title_column_fr].tolist()
            file_ids_it = sampled_df_it[file_id_column_diverse].tolist()
            titles_it = sampled_df_it[title_column_diverse].tolist()

            for idx_pair in common_indices:
                fr_idx = idx_pair[0]
                it_idx = idx_pair[1]
                file_id_fr = file_ids_fr[fr_idx]
                title_fr = titles_fr[fr_idx]
                file_id_it = file_ids_it[it_idx]
                title_it = titles_it[it_idx]
                similarity_score = similarity_matrix[fr_idx, it_idx]
                common_documents.append((file_id_fr, title_fr, file_id_it, title_it, similarity_score))

    df = pd.DataFrame(common_documents, columns=['file_id_fr', 'title_fr', 'file_id_PD_Diverse', 'title_PD_Diverse', 'similarity_score'])
    df.to_csv(output_csv, index=False)
    print("Common French_PD/French_PD Diverse documents:")
    for file_id_fr, title_fr, file_id_it, title_it, similarity_score in common_documents:
        print(f"French_PD document '{file_id_fr}' ({title_fr}) is similar to French_PD_Diverse document '{file_id_it}' ({title_it}) with similarity score {similarity_score}")
    end_time = time.time()
    duration = end_time - start_time
    print(f"Deduplication process took {duration} seconds.")
    return duration

cross_lingual_start_time = time.time()
cross_lingual_similarity(parquet_dir_fr, parquet_dir_diverse, threshold=0.8, file_id_column_fr='file_id', title_column_fr='title', file_id_column_diverse='identifier', title_column_diverse='title')

cross_lingual_end_time = time.time()
cross_lingual_total_duration = cross_lingual_end_time - cross_lingual_start_time
print(f"Total cross-lingual similarity process took {cross_lingual_total_duration} seconds.")
append_duration_to_csv(output_csv_cross_lingual, 'Cross-lingual Deduplication', cross_lingual_total_duration)



