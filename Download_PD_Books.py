import os
from tqdm import tqdm
from huggingface_hub import HfApi, hf_hub_download

def download_files(directory, repo_id, subfolder, parquet_files, access_token):
    """
    Downloads the specified parquet files from the given repo and saves them in the given directory.

    Parameters:
        directory (str): Directory path where files will be saved.
        repo_id (str): The repository ID.
        subfolder (str): The subfolder in the repository to download from.
        parquet_files (list): List of parquet file names to download.
        access_token (str): The access token for private repositories.
    """

    # Create the directory if it does not exist
    if not os.path.exists(directory):
        os.makedirs(directory)

    # Loop through the parquet files
    for file_path in tqdm(parquet_files):
        try:
            hf_hub_download(
                repo_id=repo_id,
                filename=file_path,
                repo_type="dataset",
                token=access_token,
                local_dir=directory,
                local_dir_use_symlinks=False,
                #cache_dir="/mnt/jupiter/cache"
            )
            print(f"Downloaded: {file_path}")
        except Exception as e:
            print(f"Error downloading {file_path}: {str(e)}")

    print(f"All {len(parquet_files)} files downloaded successfully.")

# Initialize the Hugging Face API client
api = HfApi()

# Specify the dataset repository and subfolder
repo_id = "PleIAs/French-PD-Books"
subfolder = "admin_sites_web"

# Your access token (keep this secure!)
access_token = "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"

all_files = api.list_repo_files(repo_id, repo_type="dataset", token=access_token)

# Filter for parquet files in the specific subfolder
#parquet_files = [file for file in all_files 
#                 if file.startswith(subfolder) and file.endswith('.parquet')]

parquet_files = [file for file in all_files 
                 if file.endswith('.parquet')]

# Print the parquet files
print(f"Parquet files in the subfolder '{subfolder}':")
for file in parquet_files:
    print(file)

# Print the total count
print(f"\nTotal number of parquet files: {len(parquet_files)}")

# Set up parameters for downloading
directory = "/lus/work/CT10/c1614990/pclanglais/PD_Books"

# Download the files
download_files(directory, repo_id, subfolder, parquet_files, access_token)