import os

def get_all_parquet_files(directory: str) -> list[str]:
    parquet_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".parquet"):
                parquet_files.append(os.path.join(root, file))
    return parquet_files

def main():
    # Define input directory for Mariana dataset
    input_directory = "/gpfsdswork/projects/rech/fmr/uft12cr/corpus/marianne_opendata/admin_sites_pdf_doc"

    # Get all parquet files
    all_files = get_all_parquet_files(input_directory)

    # Calculate total number of files
    total_files = len(all_files)

    # Print results
    print(f"Total number of parquet files: {total_files}")

    # Calculate start indices for 2 jobs, each processing 50% of the files
    files_per_job = total_files // 2
    start_indices = [0, files_per_job]

    print("\nSuggested parameters for 2 jobs (50% each):")
    for i, start_index in enumerate(start_indices, 1):
        print(f"Job {i}: --files_percentage=0.5 --start_index={start_index}")

    print("\nExample of how these parameters would be used:")
    for start_index in start_indices:
        num_files_to_process = int(total_files * 0.5)
        end_index = start_index + num_files_to_process
        print(f"Start index: {start_index}, End index: {end_index}, Files to process: {num_files_to_process}")

if __name__ == "__main__":
    main()