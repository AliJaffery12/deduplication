import os
import pandas as pd
from collections import defaultdict
import traceback

base_path = '/gpfsdswork/projects/rech/fmr/uft12cr/corpus/big_code_1/data'
languages = os.listdir(base_path)
language_stats = defaultdict(lambda: defaultdict(float))

try:
    for lang in languages:
        print(f"Processing language: {lang}")
        lang_path = os.path.join(base_path, lang)
        
        if not os.path.isdir(lang_path):
            print(f"Skipping {lang}: Not a directory")
            continue
        
        parquet_files = [f for f in os.listdir(lang_path) if f.endswith('.parquet')]
        
        if not parquet_files:
            print(f"No parquet files found for {lang}")
            continue
        
        for file in parquet_files:
            file_path = os.path.join(lang_path, file)
            try:
                df = pd.read_parquet(file_path)
                
                # Collect statistics
                language_stats[lang]['file_count'] += 1
                language_stats[lang]['total_size'] += df['size'].sum()
                language_stats[lang]['total_stars'] += df['max_stars_count'].sum()
                language_stats[lang]['total_issues'] += df['max_issues_count'].sum()
                language_stats[lang]['total_forks'] += df['max_forks_count'].sum()
                language_stats[lang]['avg_line_length'] += df['avg_line_length'].mean()
                language_stats[lang]['max_line_length'] = max(language_stats[lang]['max_line_length'], df['max_line_length'].max())
                language_stats[lang]['alphanum_fraction'] += df['alphanum_fraction'].mean()
                language_stats[lang]['file_count'] += len(df)
                
                print(f"  Processed {file}")
            except Exception as e:
                print(f"  Error processing {file_path}: {str(e)}")
                print(traceback.format_exc())  # This will print the full error traceback

    print("Finished processing all languages")

    # Calculate averages
    for lang in language_stats:
        file_count = language_stats[lang]['file_count']
        if file_count > 0:
            language_stats[lang]['avg_size'] = language_stats[lang]['total_size'] / file_count
            language_stats[lang]['avg_stars'] = language_stats[lang]['total_stars'] / file_count
            language_stats[lang]['avg_issues'] = language_stats[lang]['total_issues'] / file_count
            language_stats[lang]['avg_forks'] = language_stats[lang]['total_forks'] / file_count
            language_stats[lang]['avg_line_length'] /= file_count
            language_stats[lang]['alphanum_fraction'] /= file_count

    # Convert to DataFrame for easier analysis
    df_stats = pd.DataFrame(language_stats).T

    # Sort languages by different metrics
    top_languages = {
        'by_stars': df_stats.sort_values('total_stars', ascending=False).head(10).index.tolist(),
        'by_issues': df_stats.sort_values('total_issues', ascending=False).head(10).index.tolist(),
        'by_forks': df_stats.sort_values('total_forks', ascending=False).head(10).index.tolist(),
        'by_avg_line_length': df_stats.sort_values('avg_line_length', ascending=False).head(10).index.tolist(),
        'by_alphanum_fraction': df_stats.sort_values('alphanum_fraction', ascending=False).head(10).index.tolist()
    }

    # Print results
    print("\nLanguage Statistics:")
    print(df_stats.to_string())
    print("\nTop Languages:")
    for category, langs in top_languages.items():
        print(f"\n{category.replace('_', ' ').title()}:")
        for i, lang in enumerate(langs, 1):
            print(f"{i}. {lang}")

except Exception as e:
    print(f"An error occurred: {str(e)}")
    print(traceback.format_exc())

print("Script execution completed.")