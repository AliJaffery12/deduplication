class BPE:
    def __init__(self):
        self.target_vocab_size = 0
        self.vocabulary = []
        self.merge_rules = []

    def calculate_frequency(self, words):
        freq_dict = {}
        for word in words:
            if word not in freq_dict:
                freq_dict[word] = 1
            else:
                freq_dict[word] += 1
        return [(word, freq_dict[word]) for word in freq_dict.keys()]

    def create_merge_rule(self, corpus):
        pair_frequencies = self.find_pair_frequencies(corpus)
        most_frequent_pair = max(pair_frequencies, key=pair_frequencies.get)
        self.merge_rules.append(most_frequent_pair.split(','))
        self.vocabulary.append(most_frequent_pair)

    def create_vocabulary(self, words):
        return list(set(''.join(words)))

    def find_pair_frequencies(self, corpus):
        pair_freq_dict = {}
        for word, word_freq in corpus:
            for idx in range(len(word) - 1):
                char_pair = f'{word[idx]},{word[idx+1]}'
                if char_pair not in pair_freq_dict:
                    pair_freq_dict[char_pair] = word_freq
                else:
                    pair_freq_dict[char_pair] += word_freq
        return pair_freq_dict

    def get_merged_chars(self, char_1, char_2):
        return char_1 + char_2

    def initialize_corpus(self, words):
        corpus = self.calculate_frequency(words)
        return [([*word], freq) for (word, freq) in corpus]

    def merge(self, corpus):
        merge_rule = self.merge_rules[-1]
        new_corpus = []
        for word, word_freq in corpus:
            new_word = []
            idx = 0
            while idx < len(word):
                if (len(word) != 1) and (idx + 1 < len(word)) and (word[idx] == merge_rule[0]) and (word[idx+1] == merge_rule[1]):
                    new_word.append(self.get_merged_chars(word[idx], word[idx+1]))
                    idx += 2
                else:
                    new_word.append(word[idx])
                    idx += 1
            new_corpus.append((new_word, word_freq))
        return new_corpus

    def train_batch(self, words):
        corpus = self.initialize_corpus(words)
        vocabulary = self.create_vocabulary(words)

        if self.target_vocab_size and len(vocabulary) > self.target_vocab_size:
            raise TargetVocabularySizeError(f'Error: Target vocabulary size must be greater than the initial vocabulary size ({len(vocabulary)})')

        while len(vocabulary) < self.target_vocab_size:
            try:
                self.create_merge_rule(corpus)
                corpus = self.merge(corpus)
                vocabulary = list(set(vocabulary))
            except ValueError:
                print('Exiting: No further merging is possible')
                break

        self.vocabulary = list(set(self.vocabulary + vocabulary))

    def finalize_training(self, target_vocab_size):
        self.target_vocab_size = target_vocab_size
        while len(self.vocabulary) < self.target_vocab_size:
            try:
                self.create_merge_rule(self.corpus)
                self.corpus = self.merge(self.corpus)
                self.vocabulary = list(set(self.vocabulary))
            except ValueError:
                print('Exiting: No further merging is possible')
                break

    def tokenize(self, text):
        tokens = [*text]
        for merge_rule in self.merge_rules:
            new_tokens = []
            idx = 0
            while idx < len(tokens):
                if (len(tokens) != 1) and (idx + 1 < len(tokens)) and (tokens[idx] == merge_rule[0]) and (tokens[idx+1] == merge_rule[1]):
                    new_tokens.append(self.get_merged_chars(tokens[idx], tokens[idx+1]))
                    idx += 2
                else:
                    new_tokens.append(tokens[idx])
                    idx += 1
            tokens = new_tokens
        return tokens


def read_abstracts_from_file(file_path):
    with open(file_path, "r") as file:
        for line in file:
            yield line.strip()


import os
import glob

def process_all_clusters_file(file_path, target_vocab_size, batch_size=400):
    print(f'Processing all clusters file in batches: {file_path}')
    
    bpe = BPE()
    
    def read_abstracts_in_batches(file_path, batch_size):
        with open(file_path, "r") as file:
            batch = []
            for line in file:
                batch.append(line.strip())
                if len(batch) == batch_size:
                    yield batch
                    batch = []
            if batch:
                yield batch

    for batch in read_abstracts_in_batches(file_path, batch_size):
        batch_words = [word for abstract in batch for word in abstract.split()]
        bpe.train_batch(batch_words)
    
    bpe.finalize_training(target_vocab_size)
    
    # Define the path for saving the vocabulary file
    vocab_file_path = "/linkhome/rech/genrug01/uft12cr/All_clusters_vocab.txt"
    
    # Ensure the directory exists before writing the file
    os.makedirs(os.path.dirname(vocab_file_path), exist_ok=True)
    
    with open(vocab_file_path, "w") as vocab_file:
        for vocab_word in bpe.vocabulary:
            vocab_file.write(vocab_word + "\n")
    print(f'Saved vocabulary to {vocab_file_path}')
    
    return bpe



def process_cluster_files(cluster_dir, target_vocab_size, batch_size=100):
    print(f'Current working directory: {os.getcwd()}')
    cluster_dir_path = os.path.abspath(cluster_dir)
    print(f'Cluster directory path: {cluster_dir_path}')
    print(f'Files in the cluster directory: {os.listdir(cluster_dir_path)}')

    cluster_files = glob.glob(os.path.join(cluster_dir_path, "*_cluster.txt"))

    if not cluster_files:
        print(f'No cluster files found in directory: {cluster_dir_path}')
        return

    vocab_dir = os.path.join(os.path.dirname(cluster_dir_path), "vocab_Overlap")
    os.makedirs(vocab_dir, exist_ok=True)

    cluster_vocabs = {}

    for cluster_file in cluster_files:
        print(f'Processing file: {cluster_file}')
        
        bpe = BPE()

        def read_abstracts_in_batches(file_path, batch_size):
            with open(file_path, "r") as file:
                batch = []
                for line in file:
                    batch.append(line.strip())
                    if len(batch) == batch_size:
                        yield batch
                        batch = []
                if batch:
                    yield batch

        for batch in read_abstracts_in_batches(cluster_file, batch_size):
            batch_words = [word for abstract in batch for word in abstract.split()]
            bpe.train_batch(batch_words)

        bpe.finalize_training(target_vocab_size)

        vocab_file_name = os.path.basename(cluster_file).replace("_cluster.txt", "_vocab.txt")
        vocab_file_path = os.path.join(vocab_dir, vocab_file_name)
        with open(vocab_file_path, "w") as vocab_file:
            for vocab_word in bpe.vocabulary:
                vocab_file.write(vocab_word + "\n")
        print(f'Saved vocabulary to {vocab_file_path}')
        cluster_name = os.path.basename(cluster_file).replace("_cluster.txt", "")
        cluster_vocabs[cluster_name] = bpe

    return cluster_files, cluster_vocabs

def tokenize_clusters(cluster_files, cluster_vocabs):
    tokenized_clusters = {}
    for cluster_file in cluster_files:
        abstracts = read_abstracts_from_file(cluster_file)
        words = [word for abstract in abstracts for word in abstract.split()]
        cluster_name = os.path.basename(cluster_file).replace("_cluster.txt", "")
        bpe = cluster_vocabs[cluster_name]
        tokenized_texts = [bpe.tokenize(word) for word in words]
        tokenized_clusters[cluster_name] = set(token for tokens in tokenized_texts for token in tokens)
        
        print(f"Cluster {cluster_name}:")
        print(f"  Total Tokens: {len(tokenized_clusters[cluster_name])}")
        
    return tokenized_clusters

# Example usage
all_clusters_file_path = "/linkhome/rech/genrug01/uft12cr/ALL_clusters.txt"
target_vocab_size = 16000

# Process the complete dataset (All_clusters.txt) and obtain its BPE vocabulary
all_clusters_bpe = process_all_clusters_file(all_clusters_file_path, target_vocab_size)

# Process individual cluster files
#cluster_dir = "/path/to/cluster_directory"  # Change this to your cluster directory path
#cluster_files, cluster_vocabs = process_cluster_files(cluster_dir, target_vocab_size)

# Tokenize clusters using the generated BPE models
#tokenized_clusters = tokenize_clusters(cluster_files, cluster_vocabs)
