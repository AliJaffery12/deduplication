import multiprocessing as mp
import os
import random
import re
import argparse
from collections import defaultdict
from typing import Any, Callable, List, Tuple
import pandas as pd
import datasets
import numpy as np
from tqdm import tqdm

from text_dedup import logger
from text_dedup.utils import (
    CLUSTER_COLUMN, INDEX_COLUMN, DisableReferenceCount, IOArgs, MetaArgs,
    MinHashArgs, Timer, UnionFind, ngrams, optimal_param, sha1_hash,
    xxh3_16hash, xxh3_32hash
)

SEED = 42
RNG = np.random.RandomState(SEED)
NON_ALPHA = re.compile(r"\W", re.UNICODE)
datasets.logging.set_verbosity_error()
mp.set_start_method("fork", force=True)
uf = UnionFind()
SIGNATURE_COLUMN = "__signatures__"

class Args:
    def __init__(self, **kwargs):
        self.__dict__.update(kwargs)

def get_all_parquet_files(directory: str) -> List[str]:
    parquet_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".parquet"):
                parquet_files.append(os.path.join(root, file))
    return parquet_files

def load_dataset_chunk(file_path: str, column_name: str, id_column: str, chunk_size: int, start: int) -> datasets.Dataset:
    df = pd.read_parquet(file_path, engine='pyarrow', columns=[id_column, column_name])
    df = df.iloc[start:start + chunk_size]
    df = df.rename(columns={column_name: 'processed_text'})
    dataset = datasets.Dataset.from_pandas(df)
    dataset = dataset.map(lambda example, idx: {
        INDEX_COLUMN: idx + start, 
        "id_column": example[id_column], 
        "processed_text": example["processed_text"],
        "original_text": example["processed_text"]  # Keep the original text
    }, with_indices=True)
    return dataset

def embed_func(
    processed_text: str,
    idx: int,
    original_text: str,
    *,
    num_perm: int,
    ngram_size: int,
    min_length: int,
    hashranges: List[Tuple[int, int]],
    permutations: np.ndarray,
    hash_func: Callable,
    dtype: type,
    max_hash: np.uint,
    modulo_prime: np.uint,
) -> dict[str, Any]:
    a, b = permutations
    tokens: set[bytes] = {
        bytes(" ".join(t).lower(), "utf-8") for t in ngrams(NON_ALPHA.split(processed_text.lower()), ngram_size, min_length)
    }

    hashvalues: np.ndarray = np.array([hash_func(token) for token in tokens], dtype=dtype).reshape(len(tokens), 1)
    hashvalues = (hashvalues * a + b) % modulo_prime & max_hash
    masks: np.ndarray = np.full(shape=num_perm, dtype=dtype, fill_value=max_hash)
    hashvalues = np.vstack([hashvalues, masks]).min(axis=0)
    Hs: List[bytes] = [bytes(hashvalues[start:end].byteswap().data) for start, end in hashranges]
    return {
        SIGNATURE_COLUMN: Hs, 
        INDEX_COLUMN: idx, 
        "id_column": idx, 
        "processed_text": processed_text,
        "original_text": original_text
    }

def process_batch(batch: datasets.Dataset, minhash_args, HASH_RANGES, PERMUTATIONS, DTYPE, MAX_HASH, MODULO_PRIME, hash_func, io_args):
    return batch.map(
        function=embed_func,
        fn_kwargs={
            "num_perm": minhash_args.num_perm,
            "hashranges": HASH_RANGES,
            "ngram_size": minhash_args.ngram,
            "min_length": minhash_args.min_length,
            "permutations": PERMUTATIONS,
            "hash_func": hash_func,
            "dtype": DTYPE,
            "max_hash": MAX_HASH,
            "modulo_prime": MODULO_PRIME,
        },
        input_columns=["processed_text", INDEX_COLUMN, "original_text"],
        remove_columns=[col for col in batch.column_names if col not in [INDEX_COLUMN, "id_column", "processed_text", "original_text"]],
        num_proc=io_args.num_proc,
        with_indices=False,
        desc="Fingerprinting batch...",
    )

def batch_generator(all_files, column_name, id_column, batch_size):
    for file_path in all_files:
        file_size = pd.read_parquet(file_path).shape[0]
        for start in range(0, file_size, batch_size):
            yield load_dataset_chunk(file_path, column_name, id_column, batch_size, start)

def main(io_args, meta_args, minhash_args, files_percentage, start_index):
    global uf
    uf.reset()
    HASH_BITS = minhash_args.hash_bits

    HASH_CONFIG = {
        64: (np.uint64, np.uint32((1 << 32) - 1), np.uint64((1 << 61) - 1)),
        32: (np.uint32, np.uint32((1 << 32) - 1), np.uint32((1 << 32) - 5)),
        16: (np.uint16, np.uint16((1 << 16) - 1), np.uint16((1 << 16) - 15)),
    }

    DTYPE, MAX_HASH, MODULO_PRIME = HASH_CONFIG.get(HASH_BITS, HASH_CONFIG[64])

    if minhash_args.hash_func == "sha1":
        def hash_func(byte_data):
            return sha1_hash(byte_data, d=min(HASH_BITS, 32))
    elif minhash_args.hash_func == "xxh3":
        if HASH_BITS == 16:
            hash_func = xxh3_16hash
        else:
            hash_func = xxh3_32hash

    timer = Timer()

    if minhash_args.b is not None and minhash_args.r is not None:
        B, R = minhash_args.b, minhash_args.r
    else:
        B, R = optimal_param(
            minhash_args.threshold,
            minhash_args.num_perm,
            false_positive_weight=0.5,
            false_negative_weight=0.5,
        )

    HASH_RANGES = [(i * R, (i + 1) * R) for i in range(B)]
    HASH_TABLES = [defaultdict(set) for _ in range(B)]

    PERMUTATIONS = (
        RNG.randint(1, MODULO_PRIME, size=(minhash_args.num_perm,), dtype=DTYPE),
        RNG.randint(0, MODULO_PRIME, size=(minhash_args.num_perm,), dtype=DTYPE),
    )

    with timer("Total"):
        with timer("Loading"):
            logger.info("Loading dataset from parquet files...")
            all_files = get_all_parquet_files(io_args.input_path)
            logger.info(f"Number of parquet files found: {len(all_files)}")

            num_files_to_process = int(len(all_files) * files_percentage)
            end_index = start_index + num_files_to_process
            all_files_to_process = all_files[start_index:end_index]

            logger.info(f"Processing {len(all_files_to_process)} files from index {start_index} to {end_index}")
            total_records = sum(pd.read_parquet(f).shape[0] for f in all_files_to_process)

            logger.info(f"Total records to process: {total_records}")

        with timer("MinHashing"):
            for file_path in all_files_to_process:
                for ds_chunk in batch_generator([file_path], meta_args.column, meta_args.id_column, meta_args.batch_size):
                    embedded_batches = []
                    batch_size = meta_args.batch_size
                    for start in range(0, len(ds_chunk), batch_size):
                        end = min(start + batch_size, len(ds_chunk))
                        batch = ds_chunk.select(range(start, end))
                        embedded_batch = process_batch(batch, minhash_args, HASH_RANGES, PERMUTATIONS, DTYPE, MAX_HASH, MODULO_PRIME, hash_func, io_args)
                        embedded_batches.append(embedded_batch)
                    
                    embedded = datasets.concatenate_datasets(embedded_batches)
                    LEN_EMBEDDED = len(embedded)
                    NUM_SHARDS = np.ceil(LEN_EMBEDDED / meta_args.batch_size).astype(int)

                    with timer("Clustering"):
                        edges = []
                        for i in tqdm(range(0, NUM_SHARDS), dynamic_ncols=True, desc="Iterating MinHashes..."):
                            embedded_shard = embedded.shard(
                                num_shards=NUM_SHARDS,
                                index=i,
                                contiguous=True,
                                writer_batch_size=meta_args.batch_size,
                            )
                            for key, Hs in zip(embedded_shard[INDEX_COLUMN], embedded_shard[SIGNATURE_COLUMN]):
                                for i, H in enumerate(Hs):
                                    HASH_TABLES[i][H].add(key)

                        logger.info(f"Number of clusters: {len(HASH_TABLES)}")
                        for table in tqdm(HASH_TABLES, dynamic_ncols=True, desc="Clustering..."):
                            for cluster in table.values():
                                if len(cluster) <= 1:
                                    continue
                                idx = min(cluster)
                                for x in cluster:
                                    edges.append((x, idx))
                                    uf.union(x, idx)
                        logger.info(f"Number of edges: {len(set(edges))}")

                    del embedded

        with timer("Filtering"), DisableReferenceCount():
            duplicate_pairs_list = []
            for file_path in all_files_to_process:
                for ds_chunk in batch_generator([file_path], meta_args.column, meta_args.id_column, meta_args.batch_size):
                    ds_chunk_with_clusters = ds_chunk.map(
                        function=lambda record: {
                            INDEX_COLUMN: record[INDEX_COLUMN],
                            "id_column": record["id_column"],
                            CLUSTER_COLUMN: uf.find(record[INDEX_COLUMN]),
                            "original_text": record["original_text"]
                        },
                        with_indices=False,
                        num_proc=io_args.num_proc,
                        new_fingerprint=str(random.getrandbits(128)), 
                        desc="Finding clusters...",
                    )

                    duplicate_data = ds_chunk_with_clusters.filter(
                        function=lambda record: uf.find(record[INDEX_COLUMN]) != record[INDEX_COLUMN],
                        with_indices=False,
                        num_proc=io_args.num_proc,
                        desc="Filtering clusters...",
                    )

                    duplicate_pairs = defaultdict(list)
                    for record in duplicate_data:
                        cluster_id = record[CLUSTER_COLUMN]
                        duplicate_pairs[cluster_id].append((record["id_column"], record["original_text"]))

                    for cluster, records in duplicate_pairs.items():
                        if len(records) >= 2:
                            original_id, original_text = min(records, key=lambda x: x[0])
                            for duplicate_id, duplicate_text in records:
                                if duplicate_id != original_id:
                                    duplicate_pairs_list.append({
                                        "id": original_id,
                                        "duplicated_id": duplicate_id,
                                        "original_text": original_text,
                                        "duplicate_text": duplicate_text
                                    })

            logger.info(f"Number of duplicate pairs found: {len(duplicate_pairs_list)}")

            # Create CSV with only id and duplicated_id
            csv_data = [(pair['id'], pair['duplicated_id']) for pair in duplicate_pairs_list]
            duplicate_ids_df = pd.DataFrame(csv_data, columns=['id', 'duplicated_id'])

            output_file_path = os.path.join(io_args.output, f"duplicate_Mariane_opendata_pdf_doc_1.csv")
            try:
                if duplicate_ids_df.empty:
                    logger.warning("No duplicates found. Writing empty CSV with headers.")
                    pd.DataFrame(columns=["id", "duplicated_id"]).to_csv(output_file_path, index=False)
                else:
                    duplicate_ids_df.to_csv(output_file_path, index=False)
                logger.info(f"CSV file written to: {output_file_path}")
            except Exception as e:
                logger.error(f"Error writing CSV file: {str(e)}")

            # Create .txt file with sample texts
            sample_size = min(10, len(duplicate_pairs_list))
            sample_pairs = random.sample(duplicate_pairs_list, sample_size)

            txt_output_path = os.path.join(io_args.output, "sample_duplicates.txt")
            try:
                with open(txt_output_path, 'w', encoding='utf-8') as f:
                    for pair in sample_pairs:
                        f.write(f"Original (ID: {pair['id']}):\n")
                        f.write(pair['original_text'][:500] + "..." if len(pair['original_text']) > 500 else pair['original_text'])
                        f.write(f"\n\nDuplicate (ID: {pair['duplicated_id']}):\n")
                        f.write(pair['duplicate_text'][:500] + "..." if len(pair['duplicate_text']) > 500 else pair['duplicate_text'])
                        f.write("\n\n" + "-"*80 + "\n\n")
                logger.info(f"Sample duplicates written to: {txt_output_path}")
            except Exception as e:
                logger.error(f"Error writing sample duplicates file: {str(e)}")

    PAD = 32
    timer.report(logger=logger, pad=PAD)
    logger.info(f"{'Total records':<{PAD}}: {total_records}")
    logger.info(f"{'Duplicate pairs found':<{PAD}}: {len(duplicate_pairs_list)}")
    logger.info("Script completed.")

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--files_percentage", type=float, default=1.0)
    parser.add_argument("--start_index", type=int, default=0)
    args = parser.parse_args()

    io_args = Args(
        input_path="/gpfsdswork/projects/rech/fmr/uft12cr/corpus/marianne_opendata/admin_sites_pdf_doc",
        output="/gpfsdswork/projects/rech/fmr/uft12cr",

        num_proc=1,
        debug=False,
        clean_cache=True
    )

    meta_args = Args(
        column="text",
        id_column="id",
        batch_size=200
    )

    minhash_args = Args(
        num_perm=128,
        ngram=2,
        min_length=10,
        hash_func="xxh3",
        threshold=0.8,
        hash_bits=16,
        b=None,
        r=None
    )

    main(io_args, meta_args, minhash_args, args.files_percentage, args.start_index)
