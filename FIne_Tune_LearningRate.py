import json
import torch
from transformers import BertTokenizer, BertModel, BertConfig, BertForSequenceClassification
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.cluster import KMeans
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
import re
import pandas as pd
import torch.cuda.amp as amp
from torch.optim import AdamW
import numpy as np

# Paths to locally stored model and tokenizer
MODEL_PATH = '/linkhome/rech/genrug01/uft12cr/bert_Model'
TOKENIZER_PATH = '/linkhome/rech/genrug01/uft12cr/Bert_tokenizer'
General_TOKENIZER_PATH = '/gpfswork/rech/fmr/uft12cr/finetuneAli/vocab2.txt'

# Preprocessing function
def preprocess_text(text):
    text = text.lower()
    text = re.sub(r'[^a-zA-Z\s]', '', text)
    text = re.sub(r'\s+', ' ', text).strip()
    return text

# Load data
decoder = json.JSONDecoder()
data = []
with open('/linkhome/rech/genrug01/uft12cr/arxiv-metadata-oai-snapshot.json') as f:
    for line in f:
        data.append(decoder.decode(line))

# Convert to DataFrame
df = pd.DataFrame(data)

# Preprocess abstracts
df['preprocessed_abstract'] = df['abstract'].apply(preprocess_text)

# Define domain-wise categories
domain_wise_categories = {
    "Mathematics": ["math.AG", "math.AT", "math.AP", "math.CT", "math.CA", "math.CO", "math.AC", "math.CV", "math.DG", "math.DS", "math.FA", "math.GM", "math.GN", "math.GT", "math.GR", "math.HO", "math.IT", "math.KT", "math.LO", "math.MP", "math.MG", "math.NT", "math.NA", "math.OA", "math.OC", "math.PR", "math.QA", "math.RT", "math.RA", "math.SP", "math.ST", "math.SG"],
    "Computer Science": ["cs.AI", "cs.CL", "cs.CC", "cs.CE", "cs.CG", "cs.GT", "cs.CV", "cs.CY", "cs.CR", "cs.DS", "cs.DB", "cs.DL", "cs.DM", "cs.DC", "cs.ET", "cs.FL", "cs.GL", "cs.GR", "cs.AR", "cs.HC", "cs.IR", "cs.IT", "cs.LO", "cs.LG", "cs.MS", "cs.MA", "cs.MM", "cs.NI", "cs.NE", "cs.NA", "cs.OS", "cs.OH", "cs.PF", "cs.PL", "cs.RO", "cs.SI", "cs.SE", "cs.SD", "cs.SC", "cs.SY"],
    "Physics": ["physics.acc-ph", "physics.app-ph", "physics.ao-ph", "physics.atom-ph", "physics.atm-clus", "physics.bio-ph", "physics.chem-ph", "physics.class-ph", "physics.comp-ph", "physics.data-an", "physics.flu-dyn", "physics.gen-ph", "physics.geo-ph", "physics.hist-ph", "physics.ins-ph", "physics.med-ph", "physics.optics", "physics.ed-ph", "physics.soc-ph", "physics.plasm-ph", "physics.pop-ph", "physics.space-ph"],
    "Chemistry": ["nlin.AO", "nlin.CG", "nlin.CD", "nlin.SI", "nlin.PS"],
    "Statistics": ["stat.AP", "stat.CO", "stat.ML", "stat.ME", "stat.OT", "stat.TH"],
    "Biology": ["q-bio.BM", "q-bio.CB", "q-bio.GN", "q-bio.MN", "q-bio.NC", "q-bio.OT", "q-bio.PE", "q-bio.QM", "q-bio.SC", "q-bio.TO"]
}

# Assign domain to each paper
def assign_domain(categories):
    for domain, domain_categories in domain_wise_categories.items():
        if any(cat in categories for cat in domain_categories):
            return domain
    return "Other"

df['domain'] = df['categories'].apply(assign_domain)

# Perform clustering for each domain
domain_clusters = {}
for domain in domain_wise_categories.keys():
    domain_abstracts = df[df['domain'] == domain]['preprocessed_abstract'].tolist()
    if domain_abstracts:
        vectorizer = TfidfVectorizer()
        abstract_vectors = vectorizer.fit_transform(domain_abstracts)

        num_clusters = len(domain_wise_categories[domain])
        kmeans = KMeans(n_clusters=num_clusters)
        clusters = kmeans.fit_predict(abstract_vectors)

        domain_clusters[domain] = [domain_abstracts[i] for i in range(len(domain_abstracts))]

# Prepare data for fine-tuning
all_abstracts = [abstract for abstracts in domain_clusters.values() for abstract in abstracts]
all_labels = [domain for domain, abstracts in domain_clusters.items() for _ in abstracts]

# Load custom tokenizers
try:
    all_cluster_tokenizer = BertTokenizer.from_pretrained('/gpfswork/rech/fmr/uft12cr/finetuneAli/ALL_clusters_vocab2.txt', local_files_only=True)
    final_tokenizer = BertTokenizer.from_pretrained('/gpfswork/rech/fmr/uft12cr/finetuneAli/final_vocab_cluster_pref2.txt', local_files_only=True)
except OSError:
    print("Failed to load tokenizers from txt files. Trying to load from directories.")
    all_cluster_tokenizer = BertTokenizer.from_pretrained('/gpfswork/rech/fmr/uft12cr/finetuneAli/ALL_clusters_vocab2.txt', local_files_only=True)
    final_tokenizer = BertTokenizer.from_pretrained('/gpfswork/rech/fmr/uft12cr/finetuneAli/final_vocab_cluster_pref2.txt', local_files_only=True)

# Load the general BERT tokenizer
try:
    general_tokenizer = BertTokenizer.from_pretrained(General_TOKENIZER_PATH, do_lower_case=True, local_files_only=True)
except OSError:
    print("Failed to load general BERT tokenizer. Make sure the path is correct and contains the necessary files.")
    raise

# Prepare labels
le = LabelEncoder()
labels = le.fit_transform(all_labels)
num_labels = len(le.classes_)

X_train, X_test, y_train, y_test = train_test_split(all_abstracts, labels, test_size=0.2, random_state=42)

class ArXivDataset(Dataset):
    def __init__(self, texts, labels, tokenizer, max_length=512):
        self.texts = texts
        self.labels = labels
        self.tokenizer = tokenizer
        self.max_length = max_length

    def __len__(self):
        return len(self.texts)

    def __getitem__(self, idx):
        text = self.texts[idx]
        label = self.labels[idx]

        encoding = self.tokenizer.encode_plus(
            text,
            add_special_tokens=True,
            max_length=self.max_length,
            return_token_type_ids=False,
            padding='max_length',
            truncation=True,
            return_attention_mask=True,
            return_tensors='pt',
        )

        return {
            'input_ids': encoding['input_ids'].flatten(),
            'attention_mask': encoding['attention_mask'].flatten(),
            'labels': torch.tensor(label, dtype=torch.long)
        }

def create_data_loader(X, y, tokenizer, batch_size=16):
    dataset = ArXivDataset(X, y, tokenizer)
    return DataLoader(dataset, batch_size=batch_size, shuffle=True)

def setup_model(num_labels, tokenizer):
    config = BertConfig(
        vocab_size=len(tokenizer),
        hidden_size=768,
        num_hidden_layers=12,
        num_attention_heads=12,
        intermediate_size=3072,
        num_labels=num_labels
    )
    model = BertForSequenceClassification(config)
    return model

def train_epoch(model, data_loader, optimizer, device, scheduler=None, scaler=None):
    model = model.train()
    losses = []
    correct_predictions = 0

    for batch in data_loader:
        input_ids = batch['input_ids'].to(device)
        attention_mask = batch['attention_mask'].to(device)
        labels = batch['labels'].to(device)

        with amp.autocast(enabled=scaler is not None):
            outputs = model(
                input_ids=input_ids,
                attention_mask=attention_mask,
                labels=labels
            )
            loss = outputs.loss
            preds = torch.argmax(outputs.logits, dim=1)
        
        correct_predictions += torch.sum(preds == labels)
        losses.append(loss.item())

        optimizer.zero_grad()
        if scaler:
            scaler.scale(loss).backward()
            scaler.step(optimizer)
            scaler.update()
        else:
            loss.backward()
            optimizer.step()

        if scheduler:
            scheduler.step()

    return correct_predictions.double() / len(data_loader.dataset), np.mean(losses)

def eval_model(model, data_loader, device):
    model = model.eval()
    losses = []
    correct_predictions = 0

    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['labels'].to(device)

            outputs = model(
                input_ids=input_ids,
                attention_mask=attention_mask,
                labels=labels
            )
            loss = outputs.loss
            preds = torch.argmax(outputs.logits, dim=1)

            correct_predictions += torch.sum(preds == labels)
            losses.append(loss.item())

    return correct_predictions.double() / len(data_loader.dataset), np.mean(losses)

def run_training(X_train, X_test, y_train, y_test, tokenizer, num_labels):
    device = torch.device('cpu')

    train_data_loader = create_data_loader(X_train, y_train, tokenizer, batch_size=16)
    test_data_loader = create_data_loader(X_test, y_test, tokenizer, batch_size=16)

    model = setup_model(num_labels, tokenizer)
    model = model.to(device)

    optimizer = AdamW(model.parameters(), lr=5e-5, correct_bias=False)
    total_steps = len(train_data_loader) * 3
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=2, gamma=0.1)

    scaler = amp.GradScaler() if torch.cuda.is_available() else None

    for epoch in range(3):
        print(f"Epoch {epoch + 1}/{3}")
        train_acc, train_loss = train_epoch(model, train_data_loader, optimizer, device, scheduler, scaler)
        print(f"Train loss: {train_loss}, accuracy: {train_acc}")
        val_acc, val_loss = eval_model(model, test_data_loader, device)
        print(f"Validation loss: {val_loss}, accuracy: {val_acc}")

    return model, tokenizer

# Fine-tuning models with different tokenizers
tokenizers = [general_tokenizer, all_cluster_tokenizer, final_tokenizer]
tokenizer_names = ["General Tokenizer", "All Clusters Tokenizer", "Final Tokenizer"]

for tokenizer, name in zip(tokenizers, tokenizer_names):
    print(f"Training with {name}")
    run_training(X_train, X_test, y_train, y_test, tokenizer, num_labels)
