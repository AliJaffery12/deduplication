import os
import pyarrow as pa
import pyarrow.parquet as pq
import pandas as pd
import re

# Set the paths
input_path = '/gpfswork/rech/fmr/uft12cr/corpus/big_code_1/data/jupyter-notebook'
output_path = '/gpfsscratch/rech/fmr/uft12cr/corpus/code'

# Create the output directory if it doesn't exist
os.makedirs(output_path, exist_ok=True)

# Function to check if a string looks like base64 encoded image data
def is_likely_image_data(s):
    # Check if the string is long and consists only of base64 characters
    return len(s) > 100 and bool(re.match(r'^[A-Za-z0-9+/]+={0,2}$', s))

# Process each .parquet file
for filename in os.listdir(input_path):
    if filename.endswith('.parquet'):
        input_file = os.path.join(input_path, filename)
        output_file = os.path.join(output_path, filename)
        
        # Read the parquet file
        table = pq.read_table(input_file)
        df = table.to_pandas()
        
        # Check if 'content' column exists
        if 'content' in df.columns:
            # Remove rows where 'content' looks like image data
            df = df[~df['content'].apply(is_likely_image_data)]
        
        # Save the processed dataframe as a new parquet file
        pq.write_table(pa.Table.from_pandas(df), output_file)
        
        print(f"Processed {filename}")

print("All files processed and saved in", output_path)