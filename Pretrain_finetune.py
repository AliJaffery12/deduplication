import json
import os
import logging
import torch
from transformers import BertTokenizer, BertModel, BertConfig, BertForMaskedLM
from transformers import DataCollatorForLanguageModeling
from transformers import Trainer, TrainingArguments
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
import torch.cuda.amp as amp
import re

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

# Paths to locally stored model and tokenizer
MODEL_PATH = '/linkhome/rech/genrug01/uft12cr/bert_Model'
TOKENIZER_PATH = '/linkhome/rech/genrug01/uft12cr/Bert_tokenizer'

# Check the tokenizer files
tokenizer_files = os.listdir(TOKENIZER_PATH)
logger.info(f"Tokenizer files: {tokenizer_files}")

# Ensure required files are present
required_files = ['vocab.txt', 'tokenizer_config.json']
for req_file in required_files:
    if req_file not in tokenizer_files:
        raise FileNotFoundError(f"{req_file} not found in tokenizer directory")

# Check content of tokenizer_config.json
config_path = os.path.join(TOKENIZER_PATH, 'tokenizer_config.json')
with open(config_path, 'r', encoding='utf-8') as f:
    content = f.read()
    logger.info(f"Tokenizer config content: {content}")

try:
    # Initialize tokenizer
    tokenizer = BertTokenizer.from_pretrained(TOKENIZER_PATH, local_files_only=True)
    logger.info(f"Tokenizer vocabulary size: {len(tokenizer)}")
except Exception as e:
    logger.error(f"Error initializing tokenizer: {str(e)}")
    raise

# Continue with the rest of your code...

# Domain-specific pretraining
domain_corpus_files = [
    '/linkhome/rech/genrug01/uft12cr/Mathematics_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Computer_Science_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Biology_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Physics_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Chemistry_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Statistics_cluster.txt'
]

data_collator = DataCollatorForLanguageModeling(tokenizer=tokenizer, mlm=True, mlm_probability=0.15)

config = BertConfig(
    vocab_size=tokenizer.vocab_size,
    max_position_embeddings=512,
    num_hidden_layers=12,
    num_attention_heads=12,
    intermediate_size=3072,
    hidden_dropout_prob=0.1,
    attention_probs_dropout_prob=0.1
)

model = BertForMaskedLM(config=config)

pretraining_args = TrainingArguments(
    output_dir='/linkhome/rech/genrug01/uft12cr/pretrained_model',
    overwrite_output_dir=True,
    num_train_epochs=1,  # Adjust the number of epochs as needed
    per_device_train_batch_size=16,
    save_steps=10_000,
    save_total_limit=2,
    prediction_loss_only=True,
    logging_dir='./logs',
    logging_steps=100
)

# Encode the corpus files
datasets = []
for corpus_file in domain_corpus_files:
    logger.info(f"Processing file: {corpus_file}")
    try:
        with open(corpus_file, 'r', encoding='utf-8') as file:
            lines = file.readlines()
        encoded_lines = tokenizer(lines, max_length=512, padding='max_length', truncation=True, return_tensors='pt')
        datasets.append(encoded_lines)
    except Exception as e:
        logger.error(f"Error processing file {corpus_file}: {str(e)}")

combined_dataset = torch.utils.data.ConcatDataset(datasets)

trainer = Trainer(
    model=model,
    args=pretraining_args,
    data_collator=data_collator,
    train_dataset=combined_dataset
)

# Perform domain-specific pretraining
trainer.train()
trainer.save_model('/linkhome/rech/genrug01/uft12cr/pretrained_model')

# Load the pretrained model
pretrained_model = BertModel.from_pretrained('/linkhome/rech/genrug01/uft12cr/pretrained_model')

# Load and preprocess arxiv data
data = []
with open('/linkhome/rech/genrug01/uft12cr/arxiv-metadata-oai-snapshot.json', 'r', encoding='utf-8') as f:
    for line in f:
        data.append(json.loads(line))

# Extract relevant fields
abstracts = [item['abstract'] for item in data]
categories = [item['categories'] for item in data]

# Preprocess abstracts
def preprocess_text(text):
    text = text.lower()
    text = re.sub(r'[^a-zA-Z\s]', '', text)
    text = re.sub(r'\s+', ' ', text).strip()
    return text

preprocessed_abstracts = [preprocess_text(abstract) for abstract in abstracts]

# Load tokenizers
all_cluster_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/ALL_clusters_vocab.txt')
final_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/final_vocab_cluster_pref.txt')

# Function to tokenize data
label_to_id = {}

def tokenize_data(abstracts, categories, tokenizer):
    tokenized_data = []
    for abstract, category in zip(abstracts, categories):
        if category not in label_to_id:
            label_to_id[category] = len(label_to_id)
        inputs = tokenizer.encode_plus(
            abstract,
            add_special_tokens=True,
            max_length=512,
            return_attention_mask=True,
            return_tensors='pt',
            padding='max_length',
            truncation=True
        )
        tokenized_data.append({
            'input_ids': inputs['input_ids'].flatten(),
            'attention_mask': inputs['attention_mask'].flatten(),
            'label': label_to_id[category]
        })
    return tokenized_data

all_cluster_tokenized_data = tokenize_data(preprocessed_abstracts, categories, all_cluster_tokenizer)
final_tokenized_data = tokenize_data(preprocessed_abstracts, categories, final_tokenizer)

# Dataset class
class ArxivDataset(Dataset):
    def __init__(self, tokenized_data):
        self.tokenized_data = tokenized_data

    def __len__(self):
        return len(self.tokenized_data)

    def __getitem__(self, idx):
        item = self.tokenized_data[idx]
        return {
            'input_ids': item['input_ids'],
            'attention_mask': item['attention_mask'],
            'label': item['label']
        }

# Create datasets and data loaders
all_cluster_dataset = ArxivDataset(all_cluster_tokenized_data)
final_dataset = ArxivDataset(final_tokenized_data)

all_cluster_data_loader = DataLoader(all_cluster_dataset, batch_size=8, shuffle=True)
final_data_loader = DataLoader(final_dataset, batch_size=8, shuffle=True)

# Define device
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Fine-tuning function
def fine_tune_model(data_loader, tokenizer, pretrained_model):
    logger.info("Starting fine-tuning process")
    model = pretrained_model.to(device)
    classification_head = torch.nn.Linear(model.config.hidden_size, len(label_to_id)).to(device)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(list(model.parameters()) + list(classification_head.parameters()), lr=1e-5)
    scaler = amp.GradScaler()

    for epoch in range(3):
        model.train()
        total_loss = 0
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['label'].to(device)

            optimizer.zero_grad()

            try:
                with amp.autocast():
                    outputs = model(input_ids, attention_mask=attention_mask)
                    pooled_output = outputs.pooler_output
                    logits = classification_head(pooled_output)
                    loss = criterion(logits, labels)

                scaler.scale(loss).backward()
                scaler.step(optimizer)
                scaler.update()
                total_loss += loss.item()
            except Exception as e:
                logger.error(f"Error during fine-tuning: {str(e)}")
                raise

        logger.info(f'Epoch {epoch+1}, Loss: {total_loss / len(data_loader)}')

    model.eval()
    return model, classification_head

# Fine-tune the models
all_cluster_model, all_cluster_head = fine_tune_model(all_cluster_data_loader, all_cluster_tokenizer, pretrained_model)
final_model, final_head = fine_tune_model(final_data_loader, final_tokenizer, pretrained_model)

# Evaluation function
def evaluate_model(data_loader, tokenizer, model, classification_head):
    model.eval()
    classification_head.eval()

    all_predictions = []
    all_labels = []

    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['label'].to(device)

            outputs = model(input_ids, attention_mask=attention_mask)
            pooled_output = outputs.pooler_output
            logits = classification_head(pooled_output)

            predictions = torch.argmax(logits, dim=1).cpu().numpy()
            labels = labels.cpu().numpy()

            all_predictions.extend(predictions)
            all_labels.extend(labels)

    accuracy = accuracy_score(all_labels, all_predictions)
    f1 = f1_score(all_labels, all_predictions, average='weighted')
    roc_auc = roc_auc_score(all_labels, all_predictions, multi_class='ovo')

    logger.info(f'Accuracy: {accuracy}')
    logger.info(f'F1 Score: {f1}')
    logger.info(f'ROC AUC Score: {roc_auc}')

    return accuracy, f1, roc_auc

# Evaluate the models
logger.info("Evaluating all cluster model")
all_cluster_accuracy, all_cluster_f1, all_cluster_roc_auc = evaluate_model(all_cluster_data_loader, all_cluster_tokenizer, all_cluster_model, all_cluster_head)
logger.info("Evaluating final model")
final_accuracy, final_f1, final_roc_auc = evaluate_model(final_data_loader, final_tokenizer, final_model, final_head)

# Compare the results
logger.info("All Cluster Model Performance")
logger.info(f'Accuracy: {all_cluster_accuracy}, F1 Score: {all_cluster_f1}, ROC AUC Score: {all_cluster_roc_auc}')

logger.info("Final Model Performance")
logger.info(f'Accuracy: {final_accuracy}, F1 Score: {final_f1}, ROC AUC Score: {final_roc_auc}')
