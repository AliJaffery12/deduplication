import json
import torch
from transformers import BertTokenizer, BertModel, BertConfig
from torch.utils.data import Dataset, DataLoader
from sklearn.metrics import accuracy_score, f1_score, roc_auc_score
import torch.cuda.amp as amp

# Paths to locally stored model and tokenizer
MODEL_PATH = '/linkhome/rech/genrug01/uft12cr/bert_Model'
TOKENIZER_PATH = '/linkhome/rech/genrug01/uft12cr/Bert_tokenizer'

decoder = json.JSONDecoder()
data = []
with open('/linkhome/rech/genrug01/uft12cr/arxiv-metadata-oai-snapshot.json') as f:
    for line in f:
        data.append(decoder.decode(line))

# Extract relevant fields
abstracts = [item['abstract'] for item in data]
categories = [item['categories'] for item in data]

all_cluster_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/ALL_clusters_vocab.txt')
final_tokenizer = BertTokenizer.from_pretrained('/linkhome/rech/genrug01/uft12cr/final_vocab_cluster_pref.txt')

label_to_id = {}
def tokenize_data(data, tokenizer):
    tokenized_data = []
    for abstract, category in zip(abstracts, categories):
        if category not in label_to_id:
            label_to_id[category] = len(label_to_id)
        inputs = tokenizer.encode_plus(
            abstract,
            add_special_tokens=True,
            max_length=512,
            return_attention_mask=True,
            return_tensors='pt',
            padding='max_length',
            truncation=True
        )
        tokenized_data.append({
            'input_ids': inputs['input_ids'].flatten(),
            'attention_mask': inputs['attention_mask'].flatten(),
            'label': label_to_id[category]
        })
    return tokenized_data

all_cluster_tokenized_data = tokenize_data(data, all_cluster_tokenizer)
final_tokenized_data = tokenize_data(data, final_tokenizer)

class ArxivDataset(Dataset):
    def __init__(self, tokenized_data):
        self.tokenized_data = tokenized_data

    def __len__(self):
        return len(self.tokenized_data)

    def __getitem__(self, idx):
        item = self.tokenized_data[idx]
        return {
            'input_ids': item['input_ids'],
            'attention_mask': item['attention_mask'],
            'label': item['label']
        }

all_cluster_dataset = ArxivDataset(all_cluster_tokenized_data)
final_dataset = ArxivDataset(final_tokenized_data)

all_cluster_data_loader = DataLoader(all_cluster_dataset, batch_size=8, shuffle=True)  # Reduced batch size
final_data_loader = DataLoader(final_dataset, batch_size=8, shuffle=True)  # Reduced batch size

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

def fine_tune_model(data_loader, tokenizer):
    config = BertConfig(
        vocab_size=30522,
        hidden_size=768,
        num_hidden_layers=12,
        num_attention_heads=12,
        intermediate_size=3072,
        hidden_act="gelu",
        hidden_dropout_prob=0.1,
        attention_probs_dropout_prob=0.1,
        max_position_embeddings=512,
        type_vocab_size=2,
        initializer_range=0.02,
        layer_norm_eps=1e-12,
        pad_token_id=0,
        position_embedding_type="absolute",
        use_cache=True,
        classifier_dropout=None,
        model_type="bert",
        use_gradient_checkpointing=True,  # Enable gradient checkpointing
    )
    model = BertModel(config)
    model = model.to(device)  # Move model to the specified device
    classification_head = torch.nn.Linear(model.config.hidden_size, len(label_to_id)).to(device)  # Move classification head to device
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(list(model.parameters()) + list(classification_head.parameters()), lr=1e-5)
    scaler = amp.GradScaler()  # Initialize GradScaler for mixed precision training

    for epoch in range(3):
        model.train()
        total_loss = 0
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['label'].to(device)  # Simplified this line

            optimizer.zero_grad()

            with amp.autocast():  # Use autocast context for mixed precision training
                outputs = model(input_ids, attention_mask=attention_mask)
                pooled_output = outputs.pooler_output
                logits = classification_head(pooled_output)
                loss = criterion(logits, labels)

            scaler.scale(loss).backward()
            scaler.step(optimizer)
            scaler.update()
            total_loss += loss.item()

        print(f'Epoch {epoch+1}, Loss: {total_loss / len(data_loader)}')

    model.eval()
    return model, classification_head  # Return both model and classification head

all_cluster_model, all_cluster_head = fine_tune_model(all_cluster_data_loader, all_cluster_tokenizer)
final_model, final_head = fine_tune_model(final_data_loader, final_tokenizer)

def evaluate_model(model, classification_head, data_loader):
    model.eval()
    predictions = []
    true_labels = []
    with torch.no_grad():
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels_batch = batch['label'].to(device)

            outputs = model(input_ids, attention_mask=attention_mask)
            pooled_output = outputs.pooler_output
            logits = classification_head(pooled_output)
            _, predicted = torch.max(logits, 1)

            predictions.extend(predicted.cpu().numpy())
            true_labels.extend(labels_batch.cpu().numpy())

    accuracy = accuracy_score(true_labels, predictions)
    f1 = f1_score(true_labels, predictions, average='macro')
    
    # Convert predictions to one-hot encoding for ROC AUC
    y_true = torch.nn.functional.one_hot(torch.tensor(true_labels), num_classes=len(label_to_id)).numpy()
    y_pred = torch.nn.functional.one_hot(torch.tensor(predictions), num_classes=len(label_to_id)).numpy()
    
    auc_roc = roc_auc_score(y_true, y_pred, average='macro', multi_class='ovr')
    
    return accuracy, f1, auc_roc
    
all_cluster_accuracy, all_cluster_f1, all_cluster_auc_roc = evaluate_model(all_cluster_model, all_cluster_head, all_cluster_data_loader)
final_accuracy, final_f1, final_auc_roc = evaluate_model(final_model, final_head, final_data_loader)

print('All Cluster Vocab:')
print(f'Accuracy: {all_cluster_accuracy:.4f}')
print(f'F1-score: {all_cluster_f1:.4f}')
print(f'AUC-ROC: {all_cluster_auc_roc:.4f}')

print('Final Vocab:')
print(f'Accuracy: {final_accuracy:.4f}')
print(f'F1-score: {final_f1:.4f}')
print(f'AUC-ROC: {final_auc_roc:.4f}')