import pandas as pd
import os
import pyarrow.parquet as pq

# Path to the original dataset and the duplication results CSV file
parquet_dir_fr = "/gpfsdswork/dataset/CommonCorpus/PleIAs/Polish-PD"
output_csv_separate = "/gpfsdswork/projects/rech/fmr/uft12cr/Polish_PD_deduplication.csv""

# Step 1: Count the total number of unique documents in the original dataset
def get_unique_document_ids(parquet_dir, text_column='identifier'):
    unique_ids = set()
    for file_name in os.listdir(parquet_dir):
        if file_name.endswith(".parquet"):
            file_path = os.path.join(parquet_dir, file_name)
            parquet_file = pq.ParquetFile(file_path)
            for batch in parquet_file.iter_batches(batch_size=200):
                chunk = batch.to_pandas()
                unique_ids.update(chunk[text_column].unique())
    return unique_ids

# Step 2: Load the duplicated results and extract unique document identifiers involved in duplicates
def get_duplicated_document_ids(duplication_csv):
    duplications_df = pd.read_csv(duplication_csv)
    unique_duplicated_ids = pd.unique(duplications_df[['file_id1', 'file_id2']].values.ravel('K'))
    return unique_duplicated_ids

# Get the unique document identifiers from the original dataset
original_document_ids = get_unique_document_ids(parquet_dir_fr)
total_files_processed = len(original_document_ids)

# Get the unique duplicated document identifiers from the CSV file
duplicated_document_ids = get_duplicated_document_ids(output_csv_separate)
num_unique_duplicated_files = len(duplicated_document_ids)

# Step 3: Calculate the duplication ratio/percentage
duplication_ratio = num_unique_duplicated_files / total_files_processed
duplication_percentage = duplication_ratio * 100

# Print the results
print(f"Total files processed: {total_files_processed}")
print(f"Number of unique duplicated files in csv: {num_unique_duplicated_files}")
print(f"Duplication ratio Polish: {duplication_ratio:.2f}")
print(f"Duplication percentage Polish: {duplication_percentage:.2f}%")
