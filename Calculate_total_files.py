import os
import pandas as pd

def get_all_parquet_files(directory: str) -> list[str]:
    parquet_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".parquet"):
                parquet_files.append(os.path.join(root, file))
    return parquet_files

def main():
    # Define input directories
    input_french = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books"
    input_diverse = "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-diverse"

    # Get all parquet files
    french_files = get_all_parquet_files(input_french)
    diverse_files = get_all_parquet_files(input_diverse)
    all_files = french_files + diverse_files

    # Calculate total number of files and records
    total_files = len(all_files)
    total_records = sum(pd.read_parquet(f).shape[0] for f in all_files)

    # Print results
    print(f"Total number of files: {total_files}")
    print(f"Total number of records: {total_records}")

    # Calculate start indices for 5 jobs, each processing 20% of the files
    files_per_job = total_files // 5
    start_indices = [i * files_per_job for i in range(5)]

    print("\nSuggested parameters for 5 jobs (20% each):")
    for i, start_index in enumerate(start_indices, 1):
        print(f"Job {i}: --files_percentage=0.2 --start_index={start_index}")

    print("\nExample of how these parameters would be used:")
    for start_index in start_indices:
        num_files_to_process = int(total_files * 0.2)
        end_index = start_index + num_files_to_process
        print(f"Start index: {start_index}, End index: {end_index}, Files to process: {num_files_to_process}")

if __name__ == "__main__":
    main()