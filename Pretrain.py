import json
import os
import logging
import torch
from sentence_transformers import SentenceTransformer, models
from transformers import BertTokenizer, DataCollatorForLanguageModeling
from transformers import Trainer, TrainingArguments
from torch.utils.data import Dataset, DataLoader
import torch.cuda.amp as amp
import re

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

MODEL_PATH = '/linkhome/rech/genrug01/uft12cr/bert_Model'
TOKENIZER_PATH = '/linkhome/rech/genrug01/uft12cr/Bert_tokenizer'

# Initialize tokenizer
try:
    tokenizer = BertTokenizer.from_pretrained(TOKENIZER_PATH)
    logger.info(f"Tokenizer loaded from {TOKENIZER_PATH}, vocabulary size: {len(tokenizer)}")
except Exception as e:
    logger.error(f"Error initializing tokenizer: {str(e)}")
    raise

# Initialize model
try:
    model = SentenceTransformer(MODEL_PATH)
    logger.info(f"Model loaded from {MODEL_PATH}")
except Exception as e:
    logger.error(f"Error initializing model: {str(e)}")
    raise

# Define data collator
data_collator = DataCollatorForLanguageModeling(tokenizer=tokenizer, mlm=True, mlm_probability=0.15)

# Define training arguments
pretraining_args = TrainingArguments(
    output_dir='/linkhome/rech/genrug01/uft12cr/pretrained_model',
    overwrite_output_dir=True,
    num_train_epochs=1,
    per_device_train_batch_size=16,
    save_steps=10_000,
    save_total_limit=2,
    prediction_loss_only=True,
    logging_dir='./logs',
    logging_steps=100
)

# Encode the corpus files
domain_corpus_files = [
    '/linkhome/rech/genrug01/uft12cr/Mathematics_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Computer_Science_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Biology_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Physics_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Chemistry_cluster.txt',
    '/linkhome/rech/genrug01/uft12cr/Statistics_cluster.txt'
]

datasets = []
for corpus_file in domain_corpus_files:
    logger.info(f"Processing file: {corpus_file}")
    try:
        with open(corpus_file, 'r', encoding='utf-8') as file:
            lines = file.readlines()
        encoded_lines = tokenizer(lines, max_length=512, padding='max_length', truncation=True, return_tensors='pt')
        datasets.append(encoded_lines['input_ids'])
    except Exception as e:
        logger.error(f"Error processing file {corpus_file}: {str(e)}")

combined_dataset = torch.utils.data.ConcatDataset(datasets)

trainer = Trainer(
    model=model,
    args=pretraining_args,
    data_collator=data_collator,
    train_dataset=combined_dataset
)

# Perform domain-specific pretraining
trainer.train()
trainer.save_model('/linkhome/rech/genrug01/uft12cr/pretrained_model')

# Load the pretrained model for further fine-tuning
pretrained_model = SentenceTransformer('/linkhome/rech/genrug01/uft12cr/pretrained_model')

# Load and preprocess arxiv data
data = []
with open('/linkhome/rech/genrug01/uft12cr/arxiv-metadata-oai-snapshot.json', 'r', encoding='utf-8') as f:
    for line in f:
        data.append(json.loads(line))

# Extract relevant fields
abstracts = [item['abstract'] for item in data]
categories = [item['categories'] for item in data]

# Preprocess abstracts
def preprocess_text(text):
    text = text.lower()
    text = re.sub(r'[^a-zA-Z\s]', '', text)
    text = re.sub(r'\s+', ' ', text).strip()
    return text

preprocessed_abstracts = [preprocess_text(abstract) for abstract in abstracts]

# Load tokenizers from local directories
def load_tokenizer(path):
    try:
        tokenizer = BertTokenizer.from_pretrained(path)
        logger.info(f"Tokenizer loaded from {path}")
        return tokenizer
    except Exception as e:
        logger.error(f"Error loading tokenizer from {path}: {str(e)}")
        raise

all_cluster_tokenizer = load_tokenizer('/linkhome/rech/genrug01/uft12cr/ALL_clusters_vocab.txt')
final_tokenizer = load_tokenizer('/linkhome/rech/genrug01/uft12cr/final_vocab_cluster_pref.txt')

# Function to tokenize data
label_to_id = {}

def tokenize_data(abstracts, categories, tokenizer):
    tokenized_data = []
    for abstract, category in zip(abstracts, categories):
        if category not in label_to_id:
            label_to_id[category] = len(label_to_id)
        inputs = tokenizer.encode_plus(
            abstract,
            add_special_tokens=True,
            max_length=512,
            return_attention_mask=True,
            return_tensors='pt',
            padding='max_length',
            truncation=True
        )
        tokenized_data.append({
            'input_ids': inputs['input_ids'].flatten(),
            'attention_mask': inputs['attention_mask'].flatten(),
            'label': label_to_id[category]
        })
    return tokenized_data

all_cluster_tokenized_data = tokenize_data(preprocessed_abstracts, categories, all_cluster_tokenizer)
final_tokenized_data = tokenize_data(preprocessed_abstracts, categories, final_tokenizer)

# Dataset class
class ArxivDataset(Dataset):
    def __init__(self, tokenized_data):
        self.tokenized_data = tokenized_data

    def __len__(self):
        return len(self.tokenized_data)

    def __getitem__(self, idx):
        item = self.tokenized_data[idx]
        return {
            'input_ids': item['input_ids'],
            'attention_mask': item['attention_mask'],
            'label': item['label']
        }

# Create datasets and data loaders
all_cluster_dataset = ArxivDataset(all_cluster_tokenized_data)
final_dataset = ArxivDataset(final_tokenized_data)

all_cluster_data_loader = DataLoader(all_cluster_dataset, batch_size=8, shuffle=True)
final_data_loader = DataLoader(final_dataset, batch_size=8, shuffle=True)

# Define device
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Fine-tuning function
def fine_tune_model(data_loader, tokenizer, pretrained_model):
    logger.info("Starting fine-tuning process")
    model = pretrained_model.to(device)
    classification_head = torch.nn.Linear(model.get_sentence_embeddings().shape[1], len(label_to_id)).to(device)
    criterion = torch.nn.CrossEntropyLoss()
    optimizer = torch.optim.Adam(list(model.parameters()) + list(classification_head.parameters()), lr=1e-5)
    scaler = amp.GradScaler()

    for epoch in range(3):
        model.train()
        total_loss = 0
        for batch in data_loader:
            input_ids = batch['input_ids'].to(device)
            attention_mask = batch['attention_mask'].to(device)
            labels = batch['label'].to(device)

            optimizer.zero_grad()

            try:
                with amp.autocast():
                    embeddings = model.encode(input_ids, convert_to_tensor=True)
                    logits = classification_head(embeddings)
                    loss = criterion(logits, labels)
                    scaler.scale(loss).backward()
                    scaler.step(optimizer)
                    scaler.update()

                    total_loss += loss.item()
            except Exception as e:
                logger.error(f"Error during training step: {str(e)}")

        avg_loss = total_loss / len(data_loader)
        logger.info(f"Epoch {epoch+1}/3, Loss: {avg_loss}")

        # Save model checkpoints after each epoch
        model_save_path = f'/linkhome/rech/genrug01/uft12cr/fine_tuned_model_epoch_{epoch+1}'
        model.save(model_save_path)

        logger.info(f"Model saved to {model_save_path}")

    logger.info("Fine-tuning completed.")

# Fine-tune on all cluster and final datasets
fine_tune_model(all_cluster_data_loader, all_cluster_tokenizer, pretrained_model)
fine_tune_model(final_data_loader, final_tokenizer, pretrained_model)
