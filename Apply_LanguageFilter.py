import os
import pandas as pd
import pyarrow.parquet as pq
from collections import defaultdict
import re
import json

base_path = "/lustre/fsn1/projects/rech/fmr/uft12cr/big_code_1/data/data"
output_path = '/lustre/fsn1/projects/rech/fmr/uft12cr/corpus/code/Language_Filters'



def categorize_language(lang, ext):
    if lang is None and (ext is None or ext == ""):
        return "Unknown"
    
    lang = str(lang).lower() if lang else ""
    ext = str(ext).lower() if ext and ext != "" else ""

    # Specific languages to filter
    markup_languages = ["html", "html_django", "html_eex", "html_erb", "html_php", "rhtml", "css", "haml", "mtml", "xml"]
    config_languages = ["nginx", "ini", "toml", "xpages", "yaml"]
    formatted_data = ["csv", "json", "json5", "jsonld"]

    if lang in markup_languages or ext in [f".{l}" for l in markup_languages]:
        return "Markup Languages"
    elif lang in config_languages or ext in [f".{l}" for l in config_languages]:
        return "Configuration Languages"
    elif lang in formatted_data or ext in [f".{l}" for l in formatted_data]:
        return "Formatted Data"
    
    return "Other Languages"

def apply_language_specific_filters(content, filename, category, lang, ext):
    # Define the categories to filter
    filter_categories = ["Markup Languages", "Configuration Languages", "Formatted Data"]
    
    # Define the languages and extensions to filter
    filter_languages = [
        "html", "html_django", "html_eex", "html_erb", "html_php", "rhtml", 
        "css", "haml", "mtml", "xml", "nginx", "ini", "toml", "xpages", "yaml",
        "csv", "json", "json5", "jsonld"
    ]
    filter_extensions = [
        ".html", ".htm", ".xhtml", ".php", ".rhtml", ".css", ".haml", ".mtml", 
        ".xml", ".nginx", ".ini", ".toml", ".xsp", ".yaml", ".yml",
        ".csv", ".json", ".json5", ".jsonld"
    ]

    # Check if the file should be filtered
    should_filter = (
        category in filter_categories or
        (lang and lang.lower() in filter_languages) or
        (ext and ext.lower() in filter_extensions)
    )

    if not should_filter:
        return True, None

    # Handle potential None values
    if content is None:
        return False, "No content"
    if filename is None:
        return False, "No filename"

    # Filter 1: Remove files with more than 512 lines
    if len(content.splitlines()) > 512:
        return False, "More than 512 lines"

    # Filter 2: Keep only files where visible text is at least 100 characters long and makes up at least 20% of the code
    visible_text = re.sub(r'<[^>]+>', '', content)  # Remove HTML tags
    if len(visible_text) < 100 or len(visible_text) / len(content) < 0.2:
        return False, "Insufficient visible text"

    # Filter 3: Keep only files with specific filenames
    filename_lower = filename.lower()
    filename_without_ext = os.path.splitext(filename_lower)[0]
    if "requirement" in filename_lower or filename_without_ext in {"readme", "notes", "todo", "description", "cmakelists"}:
        return True, None
    
    return False, "Filename doesn't match criteria"

def save_filtered_data(category, data):
    category_folder = os.path.join(output_path, category.replace(" ", "_"))
    os.makedirs(category_folder, exist_ok=True)
    
    df = pd.DataFrame(data)
    output_file = os.path.join(category_folder, f"{category.replace(' ', '_')}_filtered.parquet")
    df.to_parquet(output_file, index=False)
    print(f"Saved filtered data for {category} to {output_file}")

def analyze_folder(folder_path):
    category_counts = defaultdict(int)
    language_counts = defaultdict(int)
    extension_counts = defaultdict(int)
    filter_results = defaultdict(lambda: defaultdict(int))
    filtered_data = defaultdict(list)
    
    for file in os.listdir(folder_path):
        if file.endswith('.parquet'):
            file_path = os.path.join(folder_path, file)
            parquet_file = pq.ParquetFile(file_path)
            
            available_columns = parquet_file.schema.names
            required_columns = ['lang', 'ext', 'content']
            optional_columns = ['path']
            
            if not all(col in available_columns for col in required_columns):
                print(f"Skipping file {file_path} due to missing required columns")
                continue
            
            columns_to_read = required_columns + [col for col in optional_columns if col in available_columns]
            
            for batch in parquet_file.iter_batches(batch_size=10000, columns=columns_to_read):
                df = batch.to_pandas()
                
                for index, row in df.iterrows():
                    lang = row['lang']
                    ext = row['ext']
                    content = row['content']
                    path = row['path'] if 'path' in df.columns else file
                    
                    category = categorize_language(lang, ext)
                    filename = os.path.basename(path)

                    passed_filter, filter_reason = apply_language_specific_filters(content, filename, category, lang, ext)
                    
                    if passed_filter:
                        category_counts[category] += 1
                        language_counts[lang] += 1
                        extension_counts[ext if ext else 'None'] += 1
                        filtered_data[category].append(row)
                    else:
                        filter_results[category][filter_reason] += 1
    
    # Save filtered data
    for category, data in filtered_data.items():
        save_filtered_data(category, data)
    
    return dict(category_counts), dict(language_counts), dict(extension_counts), dict(filter_results)

# Main execution
all_folders = os.listdir(base_path)
folder_analysis = {}
filter_analysis = {}

for folder in all_folders:
    folder_path = os.path.join(base_path, folder)
    if os.path.isdir(folder_path):
        print(f"Processing folder: {folder}")
        category_counts, language_counts, extension_counts, filter_results = analyze_folder(folder_path)
        folder_analysis[folder] = {
            'categories': category_counts, 
            'languages': language_counts, 
            'extensions': extension_counts
        }
        filter_analysis[folder] = filter_results

# Print and save the results
with open('dynamic_language_analysis_results.txt', 'w') as f:
    for folder, data in folder_analysis.items():
        output = f"\nFolder: {folder}\n"
        output += "Categories:\n"
        for category, count in sorted(data['categories'].items(), key=lambda x: x[1], reverse=True):
            output += f"  {category}: {count}\n"
        
        output += "\nTop 10 Languages:\n"
        for lang, count in sorted(data['languages'].items(), key=lambda x: x[1], reverse=True)[:10]:
            lang_str = 'None' if lang is None else lang
            output += f"  {lang_str}: {count}\n"
        
        output += "\nTop 10 Extensions:\n"
        for ext, count in sorted(data['extensions'].items(), key=lambda x: x[1], reverse=True)[:10]:
            output += f"  {ext}: {count}\n"
        
        output += f"\nNumber of unique categories: {len(data['categories'])}\n"
        output += f"Number of unique languages: {len(data['languages'])}\n"
        output += f"Number of unique extensions: {len(data['extensions'])}\n"
        output += "=" * 50 + "\n"
        
        print(output)
        f.write(output)

# Save filter results
filter_results_path = os.path.join(output_path, 'filter_results.json')
with open(filter_results_path, 'w') as f:
    json.dump(filter_analysis, f, indent=2)

print(f"Filter results saved to {filter_results_path}")