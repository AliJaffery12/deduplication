import os
import random
import re
from collections import defaultdict
from typing import Any, Callable, List, Tuple

import pandas as pd
import datasets
import numpy as np
from tqdm import tqdm

from text_dedup import logger
from text_dedup.utils import (
    CLUSTER_COLUMN, INDEX_COLUMN, DisableReferenceCount, IOArgs, MetaArgs,
    MinHashArgs, Timer, UnionFind, ngrams, optimal_param, sha1_hash,
    xxh3_16hash, xxh3_32hash
)

SEED = 42
RNG = np.random.RandomState(SEED)
NON_ALPHA = re.compile(r"\W", re.UNICODE)
datasets.logging.set_verbosity_error()
uf = UnionFind()
SIGNATURE_COLUMN = "__signatures__"

DATASET_PATHS = {
    "French-PD-Books": "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-Books",
    "French-PD-diverse": "/gpfsdswork/dataset/CommonCorpus/PleIAs/French-PD-diverse"
}

OUTPUT_DIRS = {
    "French-PD-Books": "results/French_PD_Books",
    "French-PD-diverse": "results/French_PD_Diverse"
}

COLUMN_NAMES = {
    "French-PD-Books": "complete_text",
    "French-PD-diverse": "complete_text"
}

ID_COLUMNS = {
    "French-PD-Books": "file_id",
    "French-PD-diverse": "identifier"
}

def get_all_parquet_files(directory: str) -> List[str]:
    parquet_files = []
    for root, _, files in os.walk(directory):
        for file in files:
            if file.endswith(".parquet"):
                parquet_files.append(os.path.join(root, file))
    return parquet_files

def load_dataset_chunk(file_path: str, column_name: str, id_column: str, batch_size: int, start: int) -> datasets.Dataset:
    df = pd.read_parquet(file_path, engine='pyarrow', columns=[id_column, column_name])
    df = df.iloc[start:start + batch_size]
    df = df.rename(columns={id_column: 'id_column', column_name: 'complete_text'})
    dataset = datasets.Dataset.from_pandas(df)
    dataset = dataset.map(lambda example, idx: {INDEX_COLUMN: idx + start, "title": example.get("title", ""), "id_column": example["id_column"]}, with_indices=True)
    return dataset

def embed_func(
    content: str,
    title: str,
    idx: int,
    *,
    num_perm: int,
    ngram_size: int,
    min_length: int,
    hashranges: List[Tuple[int, int]],
    permutations: np.ndarray,
    hash_func: Callable,
    dtype: type,
    max_hash: np.uint,
    modulo_prime: np.uint,
) -> dict[str, Any]:
    a, b = permutations
    tokens: set[bytes] = {
        bytes(" ".join(t).lower(), "utf-8") for t in ngrams(NON_ALPHA.split(content.lower()), ngram_size, min_length)
    }

    hashvalues: np.ndarray = np.array([hash_func(token) for token in tokens], dtype=dtype).reshape(len(tokens), 1)
    hashvalues = (hashvalues * a + b) % modulo_prime & max_hash
    masks: np.ndarray = np.full(shape=num_perm, dtype=dtype, fill_value=max_hash)
    hashvalues = np.vstack([hashvalues, masks]).min(axis=0)
    Hs: List[bytes] = [bytes(hashvalues[start:end].byteswap().data) for start, end in hashranges]
    return {SIGNATURE_COLUMN: Hs, INDEX_COLUMN: idx, "title": title, "id_column": idx}

def process_batch(batch: datasets.Dataset, minhash_args, HASH_RANGES, PERMUTATIONS, DTYPE, MAX_HASH, MODULO_PRIME, hash_func):
    return batch.map(
        function=embed_func,
        fn_kwargs={
            "num_perm": minhash_args.num_perm,
            "hashranges": HASH_RANGES,
            "ngram_size": minhash_args.ngram,
            "min_length": minhash_args.min_length,
            "permutations": PERMUTATIONS,
            "hash_func": hash_func,
            "dtype": DTYPE,
            "max_hash": MAX_HASH,
            "modulo_prime": MODULO_PRIME,
        },
        input_columns=["complete_text", "title", INDEX_COLUMN],
        remove_columns=[col for col in batch.column_names if col not in [INDEX_COLUMN, "title", "id_column"]],
        num_proc=minhash_args.num_proc,
        with_indices=False,
        desc="Fingerprinting batch...",
    )

def batch_generator(all_files, column_name, id_column, batch_size):
    for file_path in all_files:
        file_size = pd.read_parquet(file_path).shape[0]
        for start in range(0, file_size, batch_size):
            yield load_dataset_chunk(file_path, column_name, id_column, batch_size, start)

def main(dataset_name, batch_size=200, num_proc=1):
    global uf
    uf.reset()
    HASH_BITS = 64  # Assuming 64 bits; you can parameterize if needed

    HASH_CONFIG = {
        64: (np.uint64, np.uint32((1 << 32) - 1), np.uint64((1 << 61) - 1)),
        32: (np.uint32, np.uint32((1 << 32) - 1), np.uint32((1 << 32) - 5)),
        16: (np.uint16, np.uint16((1 << 16) - 1), np.uint16((1 << 16) - 15)),
    }

    DTYPE, MAX_HASH, MODULO_PRIME = HASH_CONFIG.get(HASH_BITS, HASH_CONFIG[64])

    hash_func = xxh3_32hash if HASH_BITS > 16 else xxh3_16hash

    timer = Timer()

    B, R = optimal_param(0.8, 128)

    HASH_RANGES = [(i * R, (i + 1) * R) for i in range(B)]
    HASH_TABLES = [defaultdict(set) for _ in range(B)]

    PERMUTATIONS = (
        np.random.randint(1, MODULO_PRIME, size=(128,), dtype=DTYPE),
        np.random.randint(0, MODULO_PRIME, size=(128,), dtype=DTYPE),
    )

    dataset_path = DATASET_PATHS[dataset_name]
    output_dir = OUTPUT_DIRS[dataset_name]
    column_name = COLUMN_NAMES[dataset_name]
    id_column = ID_COLUMNS[dataset_name]

    with timer("Total"):
        with timer("Loading"):
            all_files = get_all_parquet_files(dataset_path)
            total_records = sum(pd.read_parquet(f).shape[0] for f in all_files)

        with timer("MinHashing"):
            ds_iter = batch_generator(all_files, column_name, id_column, batch_size)
            embedded_batches = []
            for batch in ds_iter:
                processed_batch = process_batch(batch, MinHashArgs(), HASH_RANGES, PERMUTATIONS, DTYPE, MAX_HASH, MODULO_PRIME, hash_func)
                embedded_batches.append(processed_batch)
            
            embedded = datasets.concatenate_datasets(embedded_batches)

            LEN_EMBEDDED = len(embedded)
            NUM_SHARDS = np.ceil(LEN_EMBEDDED / batch_size).astype(int)

            with timer("Clustering"):
                edges = []
                for i in tqdm(range(0, NUM_SHARDS), dynamic_ncols=True, desc="Iterating MinHashes..."):
                    embedded_shard = embedded.shard(
                        num_shards=NUM_SHARDS,
                        index=i,
                        contiguous=True,
                        writer_batch_size=batch_size,
                    )
                    for key, Hs in zip(embedded_shard[INDEX_COLUMN], embedded_shard[SIGNATURE_COLUMN]):
                        for i, H in enumerate(Hs):
                            edges.append((H, key))

                edges = sorted(edges, key=lambda edge: edge[0])

            for H, key in edges:
                uf.union(H, key)

        with timer("Saving results"):
            uf.to_disk(os.path.join(output_dir, "final_results.pkl"))

        # Delete embedded to free memory
        del embedded

if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("--dataset", type=str, required=True, help="Dataset to process (French-PD-Books or French-PD-diverse)")
    parser.add_argument("--batch_size", type=int, default=200, help="Batch size for processing datasets")
    parser.add_argument("--num_proc", type=int, default=1, help="Number of processes to use")
    args = parser.parse_args()
    main(args.dataset, batch_size=args.batch_size, num_proc=args.num_proc)
